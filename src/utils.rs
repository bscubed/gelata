//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::path::PathBuf;

use once_cell::sync::Lazy;

use crate::config::GETTEXT_PACKAGE;

pub static CACHE_DIR: Lazy<PathBuf> =
    Lazy::new(|| PathBuf::from(glib::user_cache_dir().to_str().unwrap()).join(GETTEXT_PACKAGE));

pub static CONFIG_DIR: Lazy<PathBuf> =
    Lazy::new(|| PathBuf::from(glib::user_config_dir().to_str().unwrap()).join(GETTEXT_PACKAGE));

pub static DATA_DIR: Lazy<PathBuf> =
    Lazy::new(|| PathBuf::from(glib::user_data_dir().to_str().unwrap()).join(GETTEXT_PACKAGE));

pub fn ticks_to_seconds(ticks: f64) -> f64 {
    ticks / 10000000.0
}

pub fn seconds_to_ticks(seconds: f64) -> f64 {
    seconds * 10000000.0
}

pub fn ticks_to_str(ticks: f64) -> String {
    seconds_to_str(ticks_to_seconds(ticks))
}

pub fn seconds_to_str(seconds: f64) -> String {
    let mut elements: Vec<String> = Vec::new();

    let hours_raw = seconds / 3600.0;
    let hours = hours_raw.floor() as i32;
    let minutes = ((hours_raw % 1.0) * 60.0).round() as i32;

    if hours > 0 {
        elements.push(format!("{hours}h"));
    }
    elements.push(format!("{minutes}m"));

    elements.join(" ")
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{Cell, OnceCell, RefCell};
use std::time::Duration;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{clone, closure_local, ParamSpec, Properties, SignalHandlerId, SourceId};
use gst::prelude::*;
use gtk::gdk::{Display, Key};
use gtk::{gdk, CompositeTemplate, EventControllerMotion};

use super::Backend;
use crate::models::enums::MediaStreamType;
use crate::models::objects::MediaStream;
use crate::prelude::*;
use crate::utils::{seconds_to_ticks, ticks_to_seconds};

fn disable_scroll(widget: &impl IsA<gtk::Widget>) {
    let event_controller =
        gtk::EventControllerScroll::new(gtk::EventControllerScrollFlags::VERTICAL);
    event_controller.set_propagation_phase(gtk::PropagationPhase::Capture);
    event_controller.connect_closure(
        "scroll",
        false,
        closure_local!(move |_: gtk::EventControllerScroll, _: f64, _: f64| { true }),
    );
    widget.add_controller(event_controller);
}

#[derive(Default, Debug, Clone, PartialEq)]
enum FullscreenState {
    #[default]
    Windowed,
    Fullscreen,
}

#[derive(Default, Debug, Clone, PartialEq)]
enum OverlayVisibility {
    #[default]
    Visible,
    Hidden,
}

#[allow(dead_code)]
#[derive(Default, Debug, Clone, PartialEq)]
enum PlaybackState {
    #[default]
    Idle,
    Playing,
    Paused,
    Buffering,
    Seeking,
}

const HIDE_CONTROLS_DELAY: Duration = Duration::from_secs(3);
const POSITION_UPDATE_FREQUENCY: Duration = Duration::from_millis(100);
const REPORT_FREQUENCY: Duration = Duration::from_secs(1);

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/player.ui")]
    #[properties(wrapper_type = super::Player)]
    pub struct Player {
        // Template widgets
        #[template_child]
        pub(super) video_overlay: TemplateChild<gtk::Overlay>,
        #[template_child]
        pub(super) video_frame: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) current_time: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) remaining_time: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) video_renderer: TemplateChild<gtk::Picture>,
        #[template_child]
        pub(super) seekbar: TemplateChild<gtk::Scale>,
        #[template_child]
        pub(super) play_pause_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub(super) volume: TemplateChild<gtk::VolumeButton>,
        #[template_child]
        pub(super) header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub(super) controls_revealer: TemplateChild<gtk::Revealer>,
        #[template_child]
        pub(super) controls_overlay: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) audio_list: TemplateChild<gtk::StringList>,
        #[template_child]
        pub(super) subtitle_list: TemplateChild<gtk::StringList>,
        #[template_child]
        pub(super) audio_dropdown_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) subtitle_dropdown_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) audio_dropdown: TemplateChild<gtk::DropDown>,
        #[template_child]
        pub(super) subtitle_dropdown: TemplateChild<gtk::DropDown>,
        #[template_child]
        pub(super) language_dialog: TemplateChild<adw::Dialog>,

        // Properties
        #[property(get, set)]
        pub(super) item_id: RefCell<String>,
        #[property(get, set)]
        pub(super) e_tag: RefCell<String>,
        #[property(get, set)]
        pub(super) play_session_id: RefCell<String>,
        #[property(get, set)]
        pub(super) media_source_id: RefCell<String>,
        #[property(get, set)]
        pub(super) run_time_ticks: RefCell<f64>,
        #[property(get)]
        pub(super) backend: OnceCell<Backend>,
        #[property(get, set)]
        pub(super) audio_track: Cell<i32>,
        #[property(get, set)]
        pub(super) subtitle_track: Cell<i32>,

        pub(super) media_streams: RefCell<Vec<MediaStream>>,
        pub(super) fullscreen_state: RefCell<FullscreenState>,
        pub(super) overlay_visibility: RefCell<OverlayVisibility>,
        pub(super) playback_state: RefCell<PlaybackState>,
        pub(super) overlay_timeout: RefCell<Option<SourceId>>,
        pub(super) position_timeout: RefCell<Option<SourceId>>,
        pub(super) reporting_timeout: RefCell<Option<SourceId>>,
        pub(super) last_mouse_pos: RefCell<(f64, f64)>,
        pub(super) header_hover_controller: RefCell<Option<EventControllerMotion>>,
        pub(super) controls_hover_controller: RefCell<Option<EventControllerMotion>>,
        pub(super) motion_handler_id: RefCell<Option<SignalHandlerId>>,
    }

    #[gtk::template_callbacks]
    impl Player {
        #[template_callback]
        fn toggle_playback_button(&self, _: &gtk::Button) {
            self.toggle_playback();
        }

        #[template_callback]
        fn toggle_fullscreen_button(&self, _: &gtk::Button) {
            self.toggle_fullscreen();
        }

        #[template_callback]
        fn seek_forward_clicked(&self, _: &gtk::Button) {
            self.seek_forward();
        }

        #[template_callback]
        fn seek_backward_clicked(&self, _: &gtk::Button) {
            self.seek_backward();
        }

        #[template_callback]
        fn video_clicked(&self, count: i32) {
            if count == 2 {
                self.toggle_fullscreen();
            }
        }

        #[template_callback]
        fn language_clicked(&self, _: &gtk::Button) {
            if let Some(window) = self.obj().window() {
                self.language_dialog.present(&window);
            }
        }

        #[template_callback]
        fn setup_dropdown_label(&self, object: &glib::Object) {
            if let Some(list_item) = object.downcast_ref::<gtk::ListItem>() {
                let label = gtk::Label::new(None);
                label.set_ellipsize(gtk::pango::EllipsizeMode::End);
                label.set_single_line_mode(true);
                label.set_lines(1);

                list_item.set_child(Some(&label));
            }
        }

        #[template_callback]
        fn bind_dropdown_label(&self, object: &glib::Object) {
            if let Some(list_item) = object.downcast_ref::<gtk::ListItem>() {
                if let Some(text) = list_item.item().and_downcast::<gtk::StringObject>() {
                    let label = list_item.child().and_downcast::<gtk::Label>().unwrap();
                    label.set_label(text.string().as_str());
                }
            }
        }

        #[template_callback]
        fn video_tapped(&self, count: i32) {
            if count == 1 {
                if self.overlay_visibility.borrow().clone() == OverlayVisibility::Hidden {
                    self.show_overlay();
                    self.refresh_timeout();
                } else {
                    self.hide_overlay();
                    self.invalidate_timeout(None);
                }
            }
        }

        #[template_callback]
        fn seekbar_value_changed(&self, _scroll_type: gtk::ScrollType, value: f64) -> bool {
            self.seek_to_position(value);
            false
        }

        #[template_callback]
        fn selected_audio_changed(&self, _params: ParamSpec) {
            if let Some(backend) = self.backend.get() {
                backend.set_audio_track(self.audio_dropdown.selected() as i32);
            }
        }

        #[template_callback]
        fn selected_subtitles_changed(&self, _params: ParamSpec) {
            if let Some(backend) = self.backend.get() {
                backend.set_subtitle_track(self.subtitle_dropdown.selected() as i32);
            }
        }

        pub fn setup_controllers(&self) {
            self.setup_key_controller();
            self.setup_motion_controller();
            self.setup_hover_controllers();
        }

        pub fn setup_key_controller(&self) {
            let key_controller = gtk::EventControllerKey::new();
            key_controller.set_propagation_phase(gtk::PropagationPhase::Capture);
            let self_weak = self.downgrade();
            key_controller.connect_key_pressed(move |_, key, _, _| {
                if let Some(this) = self_weak.upgrade() {
                    match key {
                        Key::space => this.toggle_playback(),
                        Key::Escape => {
                            if this.is_fullscreen() {
                                this.unmaximize();
                            }
                        },
                        Key::f | Key::F | Key::F11 => this.toggle_fullscreen(),
                        Key::Left => this.seek_backward(),
                        Key::Right => this.seek_forward(),
                        _ => return false.into(),
                    }
                    true.into()
                } else {
                    false.into()
                }
            });
            self.obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .add_controller(key_controller);
        }

        pub fn setup_motion_controller(&self) {
            let motion_controller = gtk::EventControllerMotion::new();
            motion_controller.set_propagation_phase(gtk::PropagationPhase::Capture);
            self.motion_handler_id
                .replace(Some(motion_controller.connect_motion(
                clone!(@weak self as this => move |_: &gtk::EventControllerMotion, x: f64, y: f64| {
                    let mouse_moved = this.last_mouse_pos.replace((x, y)) != (x, y);
                    if this.can_hide_overlay() && mouse_moved {
                        this.show_overlay();
                        this.refresh_timeout();
                    }
                }),
            )));
            self.obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .add_controller(motion_controller);
        }

        pub fn setup_hover_controllers(&self) {
            let header_hover_controller = gtk::EventControllerMotion::new();
            header_hover_controller.set_propagation_phase(gtk::PropagationPhase::Capture);
            header_hover_controller.connect_enter(
                clone!(@weak self as this => move |_: &gtk::EventControllerMotion, _: f64, _: f64| {
                    this.invalidate_timeout(None);
                }),
            );
            self.header_bar
                .add_controller(header_hover_controller.clone());

            let controls_hover_controller = gtk::EventControllerMotion::new();
            controls_hover_controller.set_propagation_phase(gtk::PropagationPhase::Capture);
            controls_hover_controller.connect_enter(
                clone!(@weak self as this => move |_: &gtk::EventControllerMotion, _: f64, _: f64| {
                    this.invalidate_timeout(None);
                }),
            );
            self.controls_overlay
                .add_controller(controls_hover_controller.clone());

            self.header_hover_controller
                .replace(Some(header_hover_controller));
            self.controls_hover_controller
                .replace(Some(controls_hover_controller));
        }

        pub fn setup_tracks(&self, media_streams: &Vec<MediaStream>) {
            let audio_streams: Vec<String> = media_streams
                .iter()
                .filter(|val| val.type_field == Some(MediaStreamType::Audio))
                .map(|val| val.display_title.clone().unwrap_or_default())
                .collect();
            let subtitle_streams: Vec<String> = media_streams
                .iter()
                .filter(|val| val.type_field == Some(MediaStreamType::Subtitle))
                .map(|val| val.display_title.clone().unwrap_or_default())
                .collect();

            for stream in audio_streams {
                self.audio_list.append(stream.as_str());
                self.audio_dropdown_container.set_visible(true);
            }
            for stream in subtitle_streams {
                self.subtitle_list.append(stream.as_str());
                self.subtitle_dropdown_container.set_visible(true);
            }
        }

        pub fn current_position(&self) -> f64 {
            self.obj().backend().position()
        }

        pub fn seek_forward(&self) {
            let current_position = self.current_position();
            self.seek_to_position(current_position + 10.0);
        }

        pub fn seek_backward(&self) {
            let current_position = self.current_position();
            self.seek_to_position(current_position - 10.0);
        }

        pub fn seek_to_position(&self, position: f64) {
            self.obj().backend().seek_to_position(position);
        }

        pub fn invalidate_timeout(&self, new_overlay_timeout: Option<SourceId>) {
            if let Some(source_id) = self.overlay_timeout.replace(new_overlay_timeout) {
                source_id.remove();
            }
        }

        pub fn refresh_timeout(&self) {
            let self_weak = self.downgrade();
            let closure = move || {
                let Some(this) = self_weak.upgrade() else {
                    return glib::ControlFlow::Break;
                };
                this.hide_overlay();
                this.invalidate_timeout(None);
                glib::ControlFlow::Break
            };
            let new_overlay_timeout = glib::timeout_add_local(HIDE_CONTROLS_DELAY, closure);
            self.invalidate_timeout(Some(new_overlay_timeout));
        }

        // Returns whether or not the user is hovering over important views.
        // In this context, important views are anything that can be interacted with by
        // the user. With this information ,we can prevent those views from being hidden while
        // the user might be trying to interact with that view.
        pub fn important_view_contains_pointer(&self) -> bool {
            (self.is_fullscreen() && self.header_contains_pointer())
                || self.controls_contains_pointer()
        }

        pub fn header_contains_pointer(&self) -> bool {
            let Some(header_hover_controller) = self.header_hover_controller.borrow().clone()
            else {
                return false;
            };
            header_hover_controller.contains_pointer()
        }

        pub fn controls_contains_pointer(&self) -> bool {
            let Some(controls_hover_controller) = self.controls_hover_controller.borrow().clone()
            else {
                return false;
            };
            controls_hover_controller.contains_pointer()
        }

        // The controls overlay should remain visible at all times when playback is
        // paused or not playing anything. It should only be able to be hidden when
        // the user may be trying to focus on the content.
        pub fn can_hide_overlay(&self) -> bool {
            self.get_playback_state() != PlaybackState::Idle
                && self.get_playback_state() != PlaybackState::Paused
                && !self.important_view_contains_pointer()
        }

        pub fn setup_components(&self) {
            let runtime_secs = ticks_to_seconds(self.obj().run_time_ticks());

            self.seekbar.set_range(0.0, runtime_secs);
            self.seekbar.set_increments(10.0, 30.0);
            self.seekbar.set_value(0.0);
            self.volume.set_value(1.0);

            disable_scroll(&self.seekbar.get());
            disable_scroll(&self.volume.get());
            disable_scroll(&self.volume.popup());
        }

        pub fn start_position_timeout(&self) {
            let self_weak = self.downgrade();
            let closure = move || {
                let Some(this) = self_weak.upgrade() else {
                    return glib::ControlFlow::Break;
                };

                let position = this.obj().backend().position_raw();
                let duration = this.obj().backend().duration_raw();
                let time_left = duration - position;

                this.seekbar.set_value(position.seconds_f64());
                this.current_time
                    .set_text(&format!("{:.0}", position.display()));
                this.remaining_time
                    .set_text(&format!("{:.0}", time_left.display()));

                glib::ControlFlow::Continue
            };
            let new_position_timeout = glib::timeout_add_local(POSITION_UPDATE_FREQUENCY, closure);
            if let Some(source_id) = self.position_timeout.replace(Some(new_position_timeout)) {
                source_id.remove();
            }
        }

        pub fn stop_position_timeout(&self) {
            if let Some(source_id) = self.position_timeout.replace(None) {
                source_id.remove();
            }
        }

        pub fn start_report_timeout(&self) {
            let self_weak = self.downgrade();
            let closure = move || {
                let Some(this) = self_weak.upgrade() else {
                    return glib::ControlFlow::Break;
                };

                this.report_current_progress();

                glib::ControlFlow::Continue
            };
            let new_reporting_timeout = glib::timeout_add_local(REPORT_FREQUENCY, closure);
            if let Some(source_id) = self.reporting_timeout.replace(Some(new_reporting_timeout)) {
                source_id.remove();
            }
        }

        pub fn stop_report_timeout(&self) {
            if let Some(source_id) = self.reporting_timeout.replace(None) {
                source_id.remove();
            }
        }

        pub fn report_playback_started(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.report_playing_item(self.obj().item_id()).unwrap();
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }

        pub fn report_playback_stopped(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.report_stopped_playing(self.obj().item_id()).unwrap();
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }

        pub fn report_current_progress(&self) {
            let position_secs = self.obj().backend().position_raw().seconds_f64();
            let position_ticks = seconds_to_ticks(position_secs);

            if let Some(client) = self.obj().client() {
                let request_manager = client
                    .report_playback_progress(
                        self.obj().item_id(),
                        position_ticks as i64,
                        self.obj().backend().volume() as i32,
                        self.is_paused(),
                    )
                    .unwrap();
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }

        pub(super) fn get_playback_state(&self) -> PlaybackState {
            self.playback_state.borrow().clone()
        }

        fn animate_header(&self, value: f64) {
            let target = self.header_bar.get();
            target.set_opacity(value);

            let mut allocation = target.allocation();
            let offset = (1.0 - value) * (allocation.height() as f64) * -1.0;
            allocation.set_y(offset as i32);
            target.size_allocate(&allocation, -1);

            let mut area_allocation = self.video_renderer.allocation();
            area_allocation.set_y(offset as i32);
            let scaled_height = self
                .obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .allocation()
                .height()
                - (allocation.height() + offset as i32);
            area_allocation.set_height(scaled_height);
            self.video_renderer.size_allocate(&area_allocation, -1);
        }

        pub fn is_fullscreen(&self) -> bool {
            self.fullscreen_state.borrow().clone() == FullscreenState::Fullscreen
        }

        pub fn toggle_fullscreen(&self) {
            if self.is_fullscreen() {
                self.unmaximize();
            } else {
                self.maximize();
            }
        }

        pub fn toggle_playback(&self) {
            if let Some(visible_child) = self.play_pause_stack.visible_child_name() {
                if visible_child == "play" {
                    self.play();
                } else {
                    self.pause();
                }
            };
        }

        pub fn play(&self) {
            self.obj().backend().play();
            self.play_pause_stack.set_visible_child_name("pause");
            self.playback_state.replace(PlaybackState::Playing);

            self.report_current_progress();
            self.start_position_timeout();
            self.start_report_timeout();

            // Playback resumed, hide overlay after a while
            self.refresh_timeout();
        }

        pub fn pause(&self) {
            self.obj().backend().pause();
            self.play_pause_stack.set_visible_child_name("play");
            self.playback_state.replace(PlaybackState::Paused);

            self.report_current_progress();
            self.stop_position_timeout();
            self.stop_report_timeout();

            // Paused; show overlay so user has visual indicator that something
            // has happened to the stream
            self.invalidate_timeout(None);
            self.show_overlay();
        }

        pub fn is_paused(&self) -> bool {
            return self.get_playback_state() == PlaybackState::Paused;
        }

        pub fn show_mouse(&self) {
            let cursor = gdk::Cursor::from_name("default", None);
            self.obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .set_cursor(cursor.as_ref());
        }

        pub fn hide_mouse(&self) {
            let cursor = gdk::Cursor::from_name("none", None);
            self.obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .set_cursor(cursor.as_ref());
        }

        pub fn show_header(&self) {
            let target = adw::CallbackAnimationTarget::new(
                clone!(@weak self as this => move |value: f64| this.animate_header(value)),
            );
            let starting_opacity = self.header_bar.opacity();
            let animation = adw::TimedAnimation::new(
                &self.header_bar.get(),
                starting_opacity,
                1.0,
                250,
                target,
            );
            animation.play();
        }

        pub fn hide_header(&self) {
            let target = adw::CallbackAnimationTarget::new(
                clone!(@weak self as this => move |value: f64| this.animate_header(value)),
            );
            let starting_opacity = self.header_bar.opacity();
            let animation = adw::TimedAnimation::new(
                &self.header_bar.get(),
                starting_opacity,
                0.0,
                250,
                target,
            );
            animation.play();
        }

        pub fn maximize(&self) {
            self.fullscreen_state.replace(FullscreenState::Fullscreen);
            self.obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .fullscreen();
            if self.can_hide_overlay() {
                self.hide_header();
            }
        }

        pub fn unmaximize(&self) {
            self.fullscreen_state.replace(FullscreenState::Windowed);
            self.obj()
                .window()
                .expect("Widget should be rooted before attempting to get window reference")
                .unfullscreen();
            self.show_header();

            // Edge case in which use highlights header while fullscreen and exits fullscreen
            self.refresh_timeout()
        }

        pub fn show_overlay(&self) {
            self.show_mouse();
            self.controls_revealer.set_reveal_child(true);
            if self.is_fullscreen() {
                self.show_header();
            }
            self.overlay_visibility.replace(OverlayVisibility::Visible);
        }

        pub fn hide_overlay(&self) {
            if !self.header_contains_pointer() {
                self.hide_mouse();
            }
            self.controls_revealer.set_reveal_child(false);
            if self.is_fullscreen() {
                self.hide_header();
            }
            self.overlay_visibility.replace(OverlayVisibility::Hidden);
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Player {
        const NAME: &'static str = "Player";
        type Type = super::Player;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Player {}
    impl WidgetImpl for Player {
        fn root(&self) {
            self.parent_root();

            let obj = self.obj();
            let uri = obj.get_stream_info();
            obj.setup_player(uri);

            self.report_playback_started();
            self.setup_components();
            self.setup_controllers();

            let css_provider = gtk::CssProvider::new();
            css_provider.load_from_data("picture#video{background-color:rgb(0,0,0);}");

            gtk::style_context_add_provider_for_display(
                &Display::default().expect("Could not connect to a display."),
                &css_provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );

            // Start playback
            self.play();
        }

        fn unrealize(&self) {
            self.parent_unrealize();

            self.obj().backend().stop();

            // Remove window motion controller so it doesn't leak memory outside of the player
            if let Some(signal_handler_id) = self.motion_handler_id.replace(None) {
                self.obj()
                    .window()
                    .expect("Widget should be rooted before attempting to get window reference")
                    .disconnect(signal_handler_id);
            };

            self.report_playback_stopped();
            self.invalidate_timeout(None);
            self.stop_position_timeout();
            self.stop_report_timeout();
            self.unmaximize();
            self.show_mouse();
        }
    }
    impl BoxImpl for Player {}
    impl OrientableImpl for Player {}
}

glib::wrapper! {
    pub struct Player(ObjectSubclass<imp::Player>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl Player {
    pub fn new(
        item_id: String,
        e_tag: String,
        play_session_id: String,
        media_source_id: String,
        media_streams: Vec<MediaStream>,
        run_time_ticks: f64,
        audio_track: Option<i32>,
        subtitle_track: Option<i32>,
    ) -> Self {
        let obj: Self = glib::Object::builder()
            .property("item-id", item_id)
            .property("e-tag", e_tag)
            .property("play-session-id", play_session_id)
            .property("media-source-id", media_source_id)
            .property("run-time-ticks", run_time_ticks)
            .property("audio-track", audio_track.unwrap_or_default())
            .property("subtitle-track", subtitle_track.unwrap_or_default())
            .build();

        obj.imp().setup_tracks(&media_streams);
        obj.imp().media_streams.replace(media_streams);

        obj
    }

    pub fn get_stream_info(&self) -> String {
        let client = self
            .client()
            .expect("Widget should be rooted before attempting to get client reference");
        let uri = client.get_video_stream_url(
            &self.item_id(),
            &self.e_tag(),
            Some(true),
            Some(&self.play_session_id()),
            Some(&self.media_source_id()),
        );
        info!("Playback URL: {uri}");
        uri
    }

    pub fn setup_player(&self, uri: String) {
        let backend = Backend::new(uri);
        self.imp()
            .video_renderer
            .set_paintable(Some(&backend.take_paintable()));
        self.imp().backend.set(backend).unwrap();

        self.backend()
            .connect_ready(clone!(@weak self as this => move |_| {
                this.imp().audio_dropdown.set_selected(this.audio_track() as u32);
                this.imp().subtitle_dropdown.set_selected(this.subtitle_track() as u32);
            }));
    }
}

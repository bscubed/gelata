//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::RefCell;
use std::time::Duration;

use anyhow::Error;
use glib::object::ObjectExt;
use glib::Properties;
use gst::prelude::*;
use gstreamer_play::{Play, PlayVideoOverlayVideoRenderer, PlayVideoRenderer};
use gtk::gdk;
use gtk::subclass::prelude::*;

mod imp {
    use std::sync::OnceLock;

    use glib::subclass::Signal;

    use super::*;

    #[derive(Debug, Properties)]
    #[properties(wrapper_type = super::Backend)]
    pub struct Backend {
        #[property(get, set)]
        pub(crate) uri: RefCell<String>,
        #[property(get, set)]
        pub(crate) player: RefCell<Play>,
        #[property(get, set)]
        pub(crate) paintable: RefCell<Option<gdk::Paintable>>,
        #[property(get, set)]
        pub(crate) is_hardware_accelerated: RefCell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Backend {
        const NAME: &'static str = "Backend";
        type Type = super::Backend;
        type ParentType = glib::Object;
    }

    impl Default for Backend {
        fn default() -> Self {
            Self {
                uri: Default::default(),
                player: Default::default(),
                paintable: Default::default(),
                is_hardware_accelerated: Default::default(),
            }
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Backend {
        fn signals() -> &'static [Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| {
                vec![Signal::builder("ready")
                    .flags(glib::SignalFlags::ACTION)
                    .build()]
            })
        }
    }
}

glib::wrapper! {
    pub struct Backend(ObjectSubclass<imp::Backend>);
}

impl Backend {
    pub fn new(uri: String) -> Self {
        let obj: Self = glib::Object::builder().property("uri", uri).build();

        if let Ok(renderer) = obj.build_renderer() {
            obj.build_player(renderer);
        }

        obj.setup_timeout();

        obj
    }

    fn build_renderer(&self) -> Result<impl IsA<PlayVideoRenderer>, Error> {
        let gtksink = gst::ElementFactory::make("gtk4paintablesink").build()?;
        let paintable = gtksink.property::<gdk::Paintable>("paintable");
        let is_hardware_accelerated = paintable
            .property::<Option<gdk::GLContext>>("gl-context")
            .is_some();

        let sink = if is_hardware_accelerated {
            gst::ElementFactory::make("glsinkbin")
                .property("sink", &gtksink)
                .build()?
        } else {
            let sink = gst::Bin::default();
            let convert = gst::ElementFactory::make("videoconvert").build()?;

            sink.add(&convert)?;
            sink.add(&gtksink)?;
            convert.link(&gtksink)?;

            sink.add_pad(&gst::GhostPad::with_target(
                &convert.static_pad("sink").unwrap(),
            )?)?;

            sink.upcast()
        };

        self.set_paintable(paintable);
        self.set_is_hardware_accelerated(is_hardware_accelerated);

        Ok(PlayVideoOverlayVideoRenderer::with_sink(&sink))
    }

    fn build_player(&self, renderer: impl IsA<PlayVideoRenderer>) {
        let player = Play::new(Some(renderer));
        player.set_uri(Some(self.uri().as_str()));
        self.set_player(player);
    }

    fn setup_timeout(&self) {
        let self_weak = self.downgrade();
        let closure = move || {
            let Some(this) = self_weak.upgrade() else {
                return glib::ControlFlow::Break;
            };
            if this.player().media_info().is_some() {
                this.emit_by_name::<()>("ready", &[]);
                return glib::ControlFlow::Break;
            }
            glib::ControlFlow::Continue
        };
        glib::timeout_add_local(Duration::from_millis(100), closure);
    }

    pub fn connect_ready<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("ready", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            f(&obj);
            None
        })
    }

    pub fn position_raw(&self) -> gst::ClockTime {
        self.player().position().unwrap_or_default()
    }

    pub fn position(&self) -> f64 {
        self.position_raw().seconds_f64()
    }

    pub fn duration_raw(&self) -> gst::ClockTime {
        self.player().duration().unwrap_or_default()
    }

    pub fn duration(&self) -> f64 {
        self.duration_raw().seconds_f64()
    }

    pub fn seek_to_position(&self, position: f64) {
        self.player()
            .seek(gst::ClockTime::from_seconds_f64(position));
    }

    pub fn take_paintable(&self) -> gdk::Paintable {
        self.paintable().unwrap()
    }

    pub fn play(&self) {
        self.player().play();
    }

    pub fn pause(&self) {
        self.player().pause();
    }

    pub fn stop(&self) {
        self.player().stop();
    }

    pub fn is_paused(&self) -> bool {
        self.player().pipeline().current_state() == gst::State::Paused
    }

    pub fn volume(&self) -> f64 {
        self.player().volume()
    }

    pub fn set_volume(&self, volume: f64) {
        self.player().set_volume(volume);
    }

    pub fn set_audio_track(&self, index: i32) {
        debug!("set_audio_track {:#?}", self.player().media_info());
        if let Some(media_info) = self.player().media_info() {
            let audio_streams = media_info.audio_streams();
            if index < audio_streams.len() as i32 {
                debug!("Setting audio track to {index}");
                match self.player().set_audio_track(index) {
                    Ok(_) => debug!("Success"),
                    Err(_) => debug!("Failure"),
                }
            }
        }
    }

    pub fn set_subtitle_track(&self, index: i32) {
        debug!("set_subtitle_track {:#?}", self.player().media_info());
        if let Some(media_info) = self.player().media_info() {
            let subtitle_streams = media_info.subtitle_streams();
            if index < subtitle_streams.len() as i32 {
                debug!("Setting subtitle track to {index}");
                match self.player().set_subtitle_track(index) {
                    Ok(_) => debug!("Success"),
                    Err(_) => debug!("Failure"),
                }
            }
        }
    }
}

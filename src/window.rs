//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::RefCell;

use adw::subclass::prelude::*;
use adw::NavigationView;
use anyhow::{anyhow, Result};
use glib::{clone, Properties};
use gtk::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

use crate::api::{Client, SocketManager};
use crate::data::DataManager;
use crate::models::Session;
use crate::widgets::{HomePage, OnboardPage};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/window.ui")]
    #[properties(wrapper_type = super::Window)]
    pub struct Window {
        // Template widgets
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,

        // Properties
        #[property(get, set)]
        pub client: RefCell<Client>,
        #[property(get, set)]
        pub socket: RefCell<SocketManager>,
        #[property(get, set)]
        pub navigation: RefCell<Option<NavigationView>>,
    }

    impl Window {
        // For some reason the compiler gives a warning about unused assignments for
        // page, name, and title even though they're used before being read?
        // Very strange, not sure why that's happening.
        #[allow(unused_assignments)]
        pub(crate) fn setup_ui(&self) {
            let mut page: Option<gtk::Widget> = None;
            let mut name: String = String::default();
            let mut title: String = String::default();

            if self.obj().client().is_configured() {
                self.obj().set_default_size(1280, 800);
                self.obj().set_resizable(true);

                // Start client related functions
                self.obj().start_socket();
                self.obj().update_capabilities();

                let view = HomePage::new();
                name = "home".to_string();
                title = "Home".to_string();

                self.obj().set_navigation(view.imp().navigation_view.get());

                page = Some(view.into());
            } else {
                self.obj().set_default_size(600, 650);
                self.obj().set_resizable(false);

                let view = OnboardPage::new();
                name = "login".to_string();
                title = "Login".to_string();

                self.obj().set_navigation(view.imp().navigation_view.get());

                page = Some(view.into());
            }

            if let Some(page) = page {
                self.stack
                    .add_titled(&page, Some(name.as_str()), title.as_str());
                self.stack.set_visible_child_name(name.as_str());
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Window {}
    impl WidgetImpl for Window {}
    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::Root, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gtk::Accessible, gio::ActionGroup, gio::ActionMap;
}

impl Window {
    pub fn new<P: glib::prelude::IsA<gtk::Application>>(application: &P) -> Self {
        let obj: Self = glib::Object::builder()
            .property("application", application)
            .build();

        // Ensure client is setup and available to children
        if let Err(e) = obj.setup_client() {
            warn!("Failed setting up client: {:?}", e);
            info!("Starting onboarding")
        }

        obj.imp().setup_ui();

        obj
    }

    pub fn setup_client(&self) -> Result<()> {
        if let Ok(session) = DataManager::decode_data::<Session>("session.bin") {
            let client = self.client();
            client.set_token(session.access_token);
            client.set_user(session.user);
            client.set_session_info(session.session_info);

            let hostname = session
                .hostname
                .ok_or(anyhow!("Couldn't retrieve hostname from saved session"))?;
            let port = session
                .port
                .ok_or(anyhow!("Couldn't retrieve port from saved session"))?;
            let use_https = session
                .use_https
                .ok_or(anyhow!("Couldn't retrieve use_https from saved session"))?;

            client.set_hostname(hostname);
            client.set_port(port);
            client.set_use_https(use_https);

            Ok(())
        } else {
            Err(anyhow!("Couldn't get cached session data"))
        }
    }

    pub fn update_capabilities(&self) {
        let client = self.client();
        let request_manager = client.update_capabilities().unwrap();

        request_manager.connect_success(clone!(@weak self as this => move |_, _| {
            info!("Successfully updated capabilities");
        }));

        request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
            error!("{}", error_str)
        }));

        request_manager.fetch();
    }

    pub fn start_socket(&self) {
        self.set_socket(self.client().get_socket());
        self.socket().start();

        self.socket()
            .connect_message(clone!(@weak self as this => move |_, response| {
                info!("Received message from server: {}", response);
            }));
    }
}

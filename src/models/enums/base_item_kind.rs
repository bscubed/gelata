//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize, Eq, Hash)]
pub enum BaseItemKind {
    #[default]
    AggregateFolder,
    Audio,
    AudioBook,
    BasePluginFolder,
    Book,
    BoxSet,
    Channel,
    ChannelFolderItem,
    CollectionFolder,
    Episode,
    Folder,
    Genre,
    ManualPlaylistsFolder,
    Movie,
    LiveTvChannel,
    LiveTvProgram,
    MusicAlbum,
    MusicArtist,
    MusicGenre,
    MusicVideo,
    Person,
    Photo,
    PhotoAlbum,
    Playlist,
    PlaylistsFolder,
    Program,
    Recording,
    Season,
    Series,
    Studio,
    Trailer,
    TvChannel,
    TvProgram,
    UserRootFolder,
    UserView,
    Video,
    Year,
}

impl BaseItemKind {
    pub fn plural_str(&self) -> &str {
        match self {
            BaseItemKind::AggregateFolder => "Aggregate Folders",
            BaseItemKind::Audio => "Audio",
            BaseItemKind::AudioBook => "Audio Books",
            BaseItemKind::BasePluginFolder => "Plugins",
            BaseItemKind::Book => "Books",
            BaseItemKind::BoxSet => "Box Sets",
            BaseItemKind::Channel => "Channels",
            BaseItemKind::ChannelFolderItem => "Channel Folders",
            BaseItemKind::CollectionFolder => "Collection Folders",
            BaseItemKind::Episode => "Episodes",
            BaseItemKind::Folder => "Folders",
            BaseItemKind::Genre => "Genres",
            BaseItemKind::ManualPlaylistsFolder => "Manual Playlist Folders",
            BaseItemKind::Movie => "Movies",
            BaseItemKind::LiveTvChannel => "Live TV Channels",
            BaseItemKind::LiveTvProgram => "Live TV Programs",
            BaseItemKind::MusicAlbum => "Music Albums",
            BaseItemKind::MusicArtist => "Music Artists",
            BaseItemKind::MusicGenre => "Music Genres",
            BaseItemKind::MusicVideo => "Music Videos",
            BaseItemKind::Person => "People",
            BaseItemKind::Photo => "Photos",
            BaseItemKind::PhotoAlbum => "Photo Albums",
            BaseItemKind::Playlist => "Playlists",
            BaseItemKind::PlaylistsFolder => "Playlist Folders",
            BaseItemKind::Program => "Programs",
            BaseItemKind::Recording => "Recordings",
            BaseItemKind::Season => "Seasons",
            BaseItemKind::Series => "Series",
            BaseItemKind::Studio => "Studios",
            BaseItemKind::Trailer => "Trailers",
            BaseItemKind::TvChannel => "TV Channels",
            BaseItemKind::TvProgram => "TV Programs",
            BaseItemKind::UserRootFolder => "User Root Folders",
            BaseItemKind::UserView => "User Views",
            BaseItemKind::Video => "Videos",
            BaseItemKind::Year => "Years",
        }
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct UserItemDataDto {
    pub is_favorite: Option<bool>,
    pub item_id: Option<String>,
    pub key: Option<String>,
    pub last_played_date: Option<String>,
    pub likes: Option<bool>,
    pub play_count: Option<f64>,
    pub playback_position_ticks: Option<f64>,
    pub played: Option<bool>,
    pub played_percentage: Option<f64>,
    pub rating: Option<f64>,
    pub unplayed_item_count: Option<f64>,
}

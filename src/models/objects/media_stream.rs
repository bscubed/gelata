//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use crate::models::enums::{MediaStreamType, SubtitleDeliveryMethod};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct MediaStream {
    pub aspect_ratio: Option<String>,
    pub average_frame_rate: Option<f64>,
    pub bit_depth: Option<f64>,
    pub bit_rate: Option<f64>,
    pub bl_present_flag: Option<f64>,
    pub channel_layout: Option<String>,
    pub channels: Option<f64>,
    pub codec: Option<String>,
    pub codec_tag: Option<String>,
    pub codec_time_base: Option<String>,
    pub color_primaries: Option<String>,
    pub color_range: Option<String>,
    pub color_space: Option<String>,
    pub color_transfer: Option<String>,
    pub comment: Option<String>,
    pub delivery_method: Option<SubtitleDeliveryMethod>,
    pub delivery_url: Option<String>,
    pub display_title: Option<String>,
    pub dv_bl_signal_compatibility_id: Option<f64>,
    pub dv_level: Option<f64>,
    pub dv_profile: Option<f64>,
    pub dv_version_major: Option<f64>,
    pub dv_version_minor: Option<f64>,
    pub el_present_flag: Option<f64>,
    pub height: Option<f64>,
    pub index: Option<f64>,
    pub is_a_v_c: Option<bool>,
    pub is_anamorphic: Option<bool>,
    pub is_default: Option<bool>,
    pub is_external: Option<bool>,
    pub is_external_url: Option<bool>,
    pub is_forced: Option<bool>,
    pub is_interlaced: Option<bool>,
    pub is_text_subtitle_stream: Option<bool>,
    pub language: Option<String>,
    pub level: Option<f64>,
    pub localized_default: Option<String>,
    pub localized_external: Option<String>,
    pub localized_forced: Option<String>,
    pub localized_undefined: Option<String>,
    pub nal_length_size: Option<String>,
    pub packet_length: Option<f64>,
    pub path: Option<String>,
    pub pixel_format: Option<String>,
    pub profile: Option<String>,
    pub real_frame_rate: Option<f64>,
    pub ref_frames: Option<f64>,
    pub rpu_present_flag: Option<f64>,
    pub sample_rate: Option<f64>,
    pub score: Option<f64>,
    pub supports_external_stream: Option<bool>,
    pub time_base: Option<String>,
    pub title: Option<String>,
    #[serde(rename = "Type")]
    pub type_field: Option<MediaStreamType>,
    pub video_do_vi_title: Option<String>,
    pub video_range: Option<String>,
    pub video_range_type: Option<String>,
    pub width: Option<f64>,
}

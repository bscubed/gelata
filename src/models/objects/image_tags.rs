//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ImageTags {
    pub primary: Option<String>,
    pub art: Option<String>,
    pub backdrop: Option<String>,
    pub banner: Option<String>,
    pub logo: Option<String>,
    pub thumb: Option<String>,
    pub disc: Option<String>,
    #[serde(rename = "Box")]
    pub box_field: Option<String>,
    pub screenshot: Option<String>,
    pub menu: Option<String>,
    pub chapter: Option<String>,
    pub box_rear: Option<String>,
    pub profile: Option<String>,
}

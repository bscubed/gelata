//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};
use serde_json::Value;

use super::{MediaAttachment, MediaStream};
use crate::models::enums::{
    IsoType, MediaProtocol, MediaSourceType, TransportStreamTimestamp, Video3DFormat, VideoType,
};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct MediaSourceInfo {
    pub analyze_duration_ms: Option<f64>,
    pub bitrate: Option<f64>,
    pub buffer_ms: Option<f64>,
    pub container: Option<String>,
    pub default_audio_stream_index: Option<f64>,
    pub default_subtitle_stream_index: Option<f64>,
    pub e_tag: Option<String>,
    pub encoder_path: Option<String>,
    pub encoder_protocol: Option<MediaProtocol>,
    pub formats: Option<Vec<String>>,
    pub gen_pts_input: Option<bool>,
    pub id: Option<String>,
    pub ignore_dts: Option<bool>,
    pub ignore_index: Option<bool>,
    pub is_infinite_stream: Option<bool>,
    pub is_remote: Option<bool>,
    pub iso_type: Option<IsoType>,
    pub live_stream_id: Option<String>,
    pub media_attachments: Option<Vec<MediaAttachment>>,
    pub media_streams: Option<Vec<MediaStream>>,
    pub name: Option<String>,
    pub open_token: Option<String>,
    pub path: Option<String>,
    pub protocol: Option<MediaProtocol>,
    pub read_at_native_framerate: Option<bool>,
    pub required_http_headers: Option<Value>,
    pub requires_closing: Option<bool>,
    pub requires_looping: Option<bool>,
    pub requires_opening: Option<bool>,
    pub run_time_ticks: Option<f64>,
    pub size: Option<f64>,
    pub supports_direct_play: Option<bool>,
    pub supports_direct_stream: Option<bool>,
    pub supports_probing: Option<bool>,
    pub supports_transcoding: Option<bool>,
    pub timestamp: Option<TransportStreamTimestamp>,
    pub transcoding_container: Option<String>,
    pub transcoding_sub_protocol: Option<String>,
    pub transcoding_url: Option<String>,
    #[serde(rename = "Type")]
    pub type_field: Option<MediaSourceType>,
    pub video3_d_format: Option<Video3DFormat>,
    pub video_type: Option<VideoType>,
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BaseItemPersonImageBlurHashes {
    pub art: Option<Value>,
    pub backdrop: Option<Value>,
    pub banner: Option<Value>,
    #[serde(rename = "Box")]
    pub box_field: Option<Value>,
    pub box_rear: Option<Value>,
    pub chapter: Option<Value>,
    pub disc: Option<Value>,
    pub logo: Option<Value>,
    pub menu: Option<Value>,
    pub primary: Option<Value>,
    pub profile: Option<Value>,
    pub screenshot: Option<Value>,
    pub thumb: Option<Value>,
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};
use serde_json::Value;

use super::{
    BaseItemDtoImageBlurHashes, BaseItemPerson, ChapterInfo, ExternalUrl, ImageTags,
    MediaSourceInfo, MediaStream, MediaUrl, NameGuidPair, UserItemDataDto,
};
use crate::models::enums::{
    BaseItemKind, ChannelType, CollectionType, DayOfWeek, ImageOrientation, IsoType, LocationType,
    MetadataField, PlayAccess, ProgramAudio, Video3DFormat, VideoType,
};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BaseItemDto {
    pub air_days: Option<Vec<DayOfWeek>>,
    pub air_time: Option<String>,
    pub airs_after_season_number: Option<f64>,
    pub airs_before_episode_number: Option<f64>,
    pub airs_before_season_number: Option<f64>,
    pub album: Option<String>,
    pub album_artist: Option<String>,
    pub album_artists: Option<Vec<NameGuidPair>>,
    pub album_count: Option<f64>,
    pub album_id: Option<String>,
    pub album_primary_image_tag: Option<String>,
    pub altitude: Option<f64>,
    pub aperture: Option<f64>,
    pub artist_count: Option<f64>,
    pub artist_items: Option<Vec<NameGuidPair>>,
    pub artists: Option<Vec<String>>,
    pub aspect_ratio: Option<String>,
    pub audio: Option<ProgramAudio>,
    pub backdrop_image_tags: Option<Vec<String>>,
    pub camera_make: Option<String>,
    pub camera_model: Option<String>,
    pub can_delete: Option<bool>,
    pub can_download: Option<bool>,
    pub channel_id: Option<String>,
    pub channel_name: Option<String>,
    pub channel_number: Option<String>,
    pub channel_primary_image_tag: Option<String>,
    pub channel_type: Option<ChannelType>,
    pub chapters: Option<Vec<ChapterInfo>>,
    pub child_count: Option<f64>,
    pub collection_type: Option<CollectionType>,
    pub community_rating: Option<f64>,
    pub completion_percentage: Option<f64>,
    pub container: Option<String>,
    pub critic_rating: Option<f64>,
    pub cumulative_run_time_ticks: Option<f64>,
    pub current_program: Option<Box<BaseItemDto>>,
    pub custom_rating: Option<String>,
    pub date_created: Option<String>,
    pub date_last_media_added: Option<String>,
    pub display_order: Option<String>,
    pub display_preferences_id: Option<String>,
    pub enable_media_source_display: Option<bool>,
    pub end_date: Option<String>,
    pub episode_count: Option<f64>,
    pub episode_title: Option<String>,
    pub etag: Option<String>,
    pub exposure_time: Option<f64>,
    pub external_urls: Option<Vec<ExternalUrl>>,
    pub extra_type: Option<String>,
    pub focal_length: Option<f64>,
    pub forced_sort_name: Option<String>,
    pub genre_items: Option<Vec<NameGuidPair>>,
    pub genres: Option<Vec<String>>,
    pub has_subtitles: Option<bool>,
    pub height: Option<f64>,
    pub id: String,
    pub image_blur_hashes: Option<BaseItemDtoImageBlurHashes>,
    pub image_orientation: Option<ImageOrientation>,
    pub image_tags: Option<ImageTags>,
    pub index_number: Option<f64>,
    pub index_number_end: Option<f64>,
    pub is_folder: Option<bool>,
    pub is_h_d: Option<bool>,
    pub is_kids: Option<bool>,
    pub is_live: Option<bool>,
    pub is_movie: Option<bool>,
    pub is_news: Option<bool>,
    pub is_place_holder: Option<bool>,
    pub is_premiere: Option<bool>,
    pub is_repeat: Option<bool>,
    pub is_series: Option<bool>,
    pub is_sports: Option<bool>,
    pub iso_speed_rating: Option<f64>,
    pub iso_type: Option<IsoType>,
    pub latitude: Option<f64>,
    pub local_trailer_count: Option<f64>,
    pub location_type: Option<LocationType>,
    pub lock_data: Option<bool>,
    pub locked_fields: Option<Vec<MetadataField>>,
    pub longitude: Option<f64>,
    pub media_source_count: Option<f64>,
    pub media_sources: Option<Vec<MediaSourceInfo>>,
    pub media_streams: Option<Vec<MediaStream>>,
    pub media_type: Option<String>,
    pub movie_count: Option<f64>,
    pub music_video_count: Option<f64>,
    pub name: Option<String>,
    pub number: Option<String>,
    pub official_rating: Option<String>,
    pub original_title: Option<String>,
    pub overview: Option<String>,
    pub parent_art_image_tag: Option<String>,
    pub parent_art_item_id: Option<String>,
    pub parent_backdrop_image_tags: Option<Vec<String>>,
    pub parent_backdrop_item_id: Option<String>,
    pub parent_id: Option<String>,
    pub parent_index_number: Option<f64>,
    pub parent_logo_image_tag: Option<String>,
    pub parent_logo_item_id: Option<String>,
    pub parent_primary_image_item_id: Option<String>,
    pub parent_primary_image_tag: Option<String>,
    pub parent_thumb_image_tag: Option<String>,
    pub parent_thumb_item_id: Option<String>,
    pub part_count: Option<f64>,
    pub path: Option<String>,
    pub people: Option<Vec<BaseItemPerson>>,
    pub play_access: Option<PlayAccess>,
    pub playlist_item_id: Option<String>,
    pub preferred_metadata_country_code: Option<String>,
    pub preferred_metadata_language: Option<String>,
    pub premiere_date: Option<String>,
    pub primary_image_aspect_ratio: Option<f64>,
    pub production_locations: Option<Vec<String>>,
    pub production_year: Option<f64>,
    pub program_count: Option<f64>,
    pub program_id: Option<String>,
    pub provider_ids: Option<Value>,
    pub recursive_item_count: Option<f64>,
    pub remote_trailers: Option<Vec<MediaUrl>>,
    pub run_time_ticks: Option<f64>,
    pub screenshot_image_tags: Option<Vec<String>>,
    pub season_id: Option<String>,
    pub season_name: Option<String>,
    pub series_count: Option<f64>,
    pub series_id: Option<String>,
    pub series_name: Option<String>,
    pub series_primary_image_tag: Option<String>,
    pub series_studio: Option<String>,
    pub series_thumb_image_tag: Option<String>,
    pub series_timer_id: Option<String>,
    pub server_id: Option<String>,
    pub shutter_speed: Option<f64>,
    pub software: Option<String>,
    pub song_count: Option<f64>,
    pub sort_name: Option<String>,
    pub source_type: Option<String>,
    pub special_feature_count: Option<f64>,
    pub start_date: Option<String>,
    pub status: Option<String>,
    pub studios: Option<Vec<NameGuidPair>>,
    pub supports_sync: Option<bool>,
    pub taglines: Option<Vec<String>>,
    pub tags: Option<Vec<String>>,
    pub timer_id: Option<String>,
    pub trailer_count: Option<f64>,
    #[serde(rename = "Type")]
    pub type_field: Option<BaseItemKind>,
    pub user_data: Option<UserItemDataDto>,
    pub video3_d_format: Option<Video3DFormat>,
    pub video_type: Option<VideoType>,
    pub width: Option<f64>,
}

impl BaseItemDto {
    pub fn has_backdrop(&self) -> bool {
        if let Some(backdrop_image_tags) = &self.backdrop_image_tags {
            return !backdrop_image_tags.is_empty();
        }
        false
    }

    pub fn formatted_year(&self) -> Option<String> {
        if let Some(production_year) = self.production_year {
            let production_year = production_year.to_string();
            if let Some(end_date) = &self.end_date {
                let end_year = &end_date.as_str()[0..4];
                return Some(production_year + " - " + end_year);
            } else {
                if self.type_field == Some(BaseItemKind::Series) {
                    return Some(production_year + " - Present");
                } else {
                    return Some(production_year);
                }
            }
        }
        None
    }
}

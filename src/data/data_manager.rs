//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::error::Error;
use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;

use crate::models::enums::ImageType;
use crate::utils::{CACHE_DIR, CONFIG_DIR, DATA_DIR};

pub struct DataManager {}

impl DataManager {
    fn encode<T: ?Sized>(value: &T, file_path: PathBuf) -> Result<String, Box<dyn Error>>
    where
        T: serde::Serialize,
    {
        let encoded: Vec<u8> = bincode::serialize(value)?;
        let mut file = File::create(file_path.clone())?;
        file.write_all(&encoded)?;
        Ok(file_path.into_os_string().into_string().unwrap())
    }

    fn decode<T>(file_path: PathBuf) -> Result<T, Box<dyn Error>>
    where
        T: serde::de::DeserializeOwned,
    {
        let mut file = File::open(file_path)?;
        let mut file_contents = Vec::new();
        let _ = file.read_to_end(&mut file_contents)?;
        let object: T = bincode::deserialize(&file_contents)?;
        Ok(object)
    }

    pub fn encode_cache<T: ?Sized>(value: &T, file_name: &str) -> Result<String, Box<dyn Error>>
    where
        T: serde::Serialize,
    {
        Self::encode(value, CACHE_DIR.join(file_name))
    }

    pub fn decode_cache<T>(file_name: &str) -> Result<T, Box<dyn Error>>
    where
        T: serde::de::DeserializeOwned,
    {
        Self::decode(CACHE_DIR.join(file_name))
    }

    pub fn encode_config<T: ?Sized>(value: &T, file_name: &str) -> Result<String, Box<dyn Error>>
    where
        T: serde::Serialize,
    {
        Self::encode(value, CONFIG_DIR.join(file_name))
    }

    pub fn decode_config<T>(file_name: &str) -> Result<T, Box<dyn Error>>
    where
        T: serde::de::DeserializeOwned,
    {
        Self::decode(CONFIG_DIR.join(file_name))
    }

    pub fn encode_data<T: ?Sized>(value: &T, file_name: &str) -> Result<String, Box<dyn Error>>
    where
        T: serde::Serialize,
    {
        Self::encode(value, DATA_DIR.join(file_name))
    }

    pub fn decode_data<T>(file_name: &str) -> Result<T, Box<dyn Error>>
    where
        T: serde::de::DeserializeOwned,
    {
        Self::decode(DATA_DIR.join(file_name))
    }

    pub fn get_image_path(item_id: String, image_type: ImageType) -> PathBuf {
        let image_type_str = image_type.to_string();
        let file_name = format!("{}.jpg", item_id);
        CACHE_DIR.join(image_type_str).join(file_name)
    }

    pub fn save_image<C>(image_path: PathBuf, image_data: C) -> std::io::Result<()>
    where
        C: AsRef<[u8]>,
    {
        std::fs::create_dir_all(image_path.clone().parent().unwrap()).unwrap();
        std::fs::write(image_path, image_data)
    }
}

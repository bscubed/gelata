//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use adw::NavigationView;
use glib::object::{CastNone, IsA};
use gtk::prelude::*;
use gtk::Widget;

use crate::api::Client;
use crate::window::Window;

pub trait WindowExt {
    fn window(&self) -> Option<Window>;
}

impl<O: IsA<Widget>> WindowExt for O {
    fn window(&self) -> Option<Window> {
        self.root().and_downcast::<Window>()
    }
}

pub trait ClientExt {
    fn client(&self) -> Option<Client>;
}

impl<O: IsA<Widget>> ClientExt for O {
    fn client(&self) -> Option<Client> {
        self.window().and_then(|window| Some(window.client()))
    }
}

pub trait NavigationExt {
    fn navigation(&self) -> Option<NavigationView>;
}

impl<O: IsA<Widget>> NavigationExt for O {
    fn navigation(&self) -> Option<NavigationView> {
        self.window().and_then(|window| window.navigation())
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod api;
pub mod application;
pub mod config;
pub mod data;
pub mod models;
pub mod player;
pub mod prelude;
pub mod utils;
pub mod widgets;
pub mod window;

use std::env::set_var;

use config::*;
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::prelude::*;
use gtk::{gio, glib};
use utils::{CACHE_DIR, CONFIG_DIR, DATA_DIR};

use self::application::GelataApplication;

extern crate pretty_env_logger;
#[macro_use]
extern crate log;

fn setup_directories() -> Result<(), String> {
    let dirs = [&*CACHE_DIR, &*CONFIG_DIR, &*DATA_DIR];

    for dir in &dirs {
        if std::fs::create_dir_all(dir.as_path()).is_err() {
            return Err(format!("Failed to create path: {}", dir.to_str().unwrap()));
        }
    }
    Ok(())
}

fn main() -> glib::ExitCode {
    set_var(
        "RUST_LOG",
        "trace,reqwest=info,tungstenite=info,video=debug,hyper_util=info",
    );
    set_var("GSK_RENDERER", "gl");

    pretty_env_logger::init();
    // Set up gettext translations
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Load resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/dev.bscubed.Gelata.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    // Inisialize gstreamer
    gst::init().unwrap();
    gtk::init().unwrap();

    gstgtk4::plugin_register_static().expect("Failed to register gstgtk4 plugin");

    if let Err(e) = setup_directories() {
        error!("{e}");
        return glib::ExitCode::FAILURE;
    }

    // Create a new GtkApplication. The application manages our main loop,
    // application windows, integration with the window manager/compositor, and
    // desktop features such as file opening and single-instance applications.
    let app = GelataApplication::new("dev.bscubed.Gelata", &gio::ApplicationFlags::empty());

    // Run the application. This function will block until the application
    // exits. Upon return, we have our exit code to return to the shell. (This
    // is the code you see when you do `echo $?` after running a command in a
    // terminal.
    app.run()
}

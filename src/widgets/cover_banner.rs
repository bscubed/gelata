//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{Cell, RefCell};

use glib::Properties;
use gtk::glib::{self};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::Widget;

mod imp {
    use super::*;

    #[derive(Default, Debug, Properties)]
    #[properties(wrapper_type = super::CoverBanner)]
    pub struct CoverBanner {
        #[property(get, set)]
        pub banner_height: Cell<i32>,
        #[property(get, set)]
        pub cover_width: Cell<i32>,
        #[property(get, set)]
        pub cover_aspect_ratio: Cell<f32>,
        #[property(get, set)]
        pub cover_margin: Cell<i32>,

        #[property(get, set)]
        pub banner: RefCell<Option<Widget>>,
        #[property(get, set)]
        pub cover: RefCell<Option<Widget>>,
        #[property(get, set)]
        pub title: RefCell<Option<Widget>>,
    }

    impl CoverBanner {
        pub fn visible_children(&self) -> Vec<gtk::Widget> {
            let mut children: Vec<gtk::Widget> = Vec::new();
            let mut optional = self.obj().upcast_ref::<gtk::Widget>().first_child();
            while let Some(child) = optional {
                if child.should_layout() {
                    children.push(child.clone());
                }
                optional = child.next_sibling();
            }
            children
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CoverBanner {
        const NAME: &'static str = "CoverBanner";
        type Type = super::CoverBanner;
        type ParentType = gtk::Widget;
        type Interfaces = (gtk::Buildable,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for CoverBanner {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();

            obj.connect_banner_notify(|obj| {
                if let Some(banner) = obj.banner() {
                    banner.set_parent(obj);
                }
            });

            obj.connect_cover_notify(|obj| {
                if let Some(cover) = obj.cover() {
                    cover.set_parent(obj);
                }
            });

            obj.connect_title_notify(|obj| {
                if let Some(title) = obj.title() {
                    title.set_parent(obj);
                }
            });
        }

        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }
    }

    impl WidgetImpl for CoverBanner {
        fn request_mode(&self) -> gtk::SizeRequestMode {
            gtk::SizeRequestMode::HeightForWidth
        }

        fn measure(&self, orientation: gtk::Orientation, for_size: i32) -> (i32, i32, i32, i32) {
            let mut min_size = 0;
            let mut nat_size = 0;

            let obj = self.obj();

            if for_size > 0 {
                if obj.banner().is_some_and(|banner| banner.is_visible()) {
                    min_size += obj.banner_height();
                    nat_size += obj.banner_height();
                }

                if obj.cover().is_some_and(|cover| cover.is_visible()) {
                    if let Some(title) = obj.title() {
                        let available_width = for_size - (obj.cover_width() + obj.cover_margin());

                        let (child_min, child_nat, _, _) =
                            title.measure(orientation, available_width);
                        min_size += child_min;
                        nat_size += child_nat;
                    }

                    let cover_height = (obj.cover_width() as f32 * obj.cover_aspect_ratio()) as i32;
                    min_size = min_size.max(cover_height);
                    nat_size = nat_size.max(cover_height);
                } else {
                    if let Some(title) = obj.title() {
                        let (child_min, child_nat, _, _) = title.measure(orientation, for_size);
                        min_size += child_min;
                        nat_size += child_nat;
                    }
                }
            }

            (min_size, nat_size, -1, -1)
        }

        fn size_allocate(&self, width: i32, height: i32, _baseline: i32) {
            let obj = self.obj();

            let mut max_y = 0;

            if let Some(banner) = obj.banner().filter(|banner| banner.is_visible()) {
                banner.size_allocate(&gtk::Allocation::new(0, 0, width, obj.banner_height()), -1);
                max_y += obj.banner_height()
            }

            if let Some(cover) = obj.cover().filter(|cover| cover.is_visible()) {
                let cover_height = (obj.cover_width() as f32 * obj.cover_aspect_ratio()) as i32;
                let y = height - cover_height;

                cover.size_allocate(
                    &gtk::Allocation::new(obj.cover_margin(), y, obj.cover_width(), cover_height),
                    -1,
                );

                if let Some(title) = obj.title() {
                    let x = obj.cover_width() + obj.cover_margin();
                    let available_width = width - x;
                    let available_height = height - max_y;

                    title.size_allocate(
                        &gtk::Allocation::new(x, max_y, available_width, available_height),
                        -1,
                    );
                }
            } else {
                if let Some(title) = obj.title() {
                    let available_height = height - max_y;
                    title.size_allocate(
                        &gtk::Allocation::new(0, max_y, width, available_height),
                        -1,
                    );
                }
            }
        }
    }

    impl BuildableImpl for CoverBanner {
        fn add_child(&self, builder: &gtk::Builder, child: &glib::Object, type_: Option<&str>) {
            let buildable = self.obj();
            let widget = child.downcast_ref::<gtk::Widget>().unwrap();

            if let Some(child_type) = type_ {
                match child_type {
                    "banner" => {
                        self.parent_add_child(builder, child, type_);
                        buildable.set_banner(widget);
                    },
                    "cover" => {
                        self.parent_add_child(builder, child, type_);
                        buildable.set_cover(widget);
                    },
                    "title" => {
                        self.parent_add_child(builder, child, type_);
                        buildable.set_title(widget);
                    },
                    _ => (),
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct CoverBanner(ObjectSubclass<imp::CoverBanner>)
        @extends gtk::Widget,
        @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Accessible;
}

impl CoverBanner {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

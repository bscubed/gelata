//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{OnceCell, RefCell};

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{clone, Properties, Value};
use gtk::{glib, CompositeTemplate};

use super::FlowGrid;
use crate::models::objects::{BaseItemDto, ListResponse};
use crate::prelude::ClientExt;
use crate::widgets::ItemCard;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/collection_page.ui")]
    #[properties(wrapper_type = super::CollectionPage)]
    pub struct CollectionPage {
        // Template widgets
        #[template_child]
        pub container: TemplateChild<gtk::Box>,
        #[template_child]
        pub header: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub mobile: TemplateChild<adw::Breakpoint>,

        // Properties
        #[property(get, set)]
        pub(super) flow_grid: OnceCell<FlowGrid>,
        #[property(get, set)]
        pub(super) collection_id: RefCell<String>,
    }

    #[gtk::template_callbacks]
    impl CollectionPage {
        pub fn setup_collection(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_items(&self.obj().collection_id()).unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    let items: ListResponse<BaseItemDto> = match serde_json::from_str::<ListResponse<BaseItemDto>>(response.as_str()) {
                        Ok(items_value) => items_value,
                        Err(e) => {
                            error!("Failed parsing items response: {:?}", e);
                            return
                        },
                    };
                    for item in items.items {
                        let child = ItemCard::new(item);
                        this.flow_grid.get().unwrap().add_child(&child)
                    };
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CollectionPage {
        const NAME: &'static str = "CollectionPage";
        type Type = super::CollectionPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for CollectionPage {}
    impl WidgetImpl for CollectionPage {
        fn root(&self) {
            self.parent_root();

            self.setup_collection()
        }
    }
    impl BoxImpl for CollectionPage {}
    impl OrientableImpl for CollectionPage {}
}

glib::wrapper! {
    pub struct CollectionPage(ObjectSubclass<imp::CollectionPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl CollectionPage {
    pub fn new(collection: &BaseItemDto) -> Self {
        let obj: Self = glib::Object::builder()
            .property("collection-id", collection.id.clone())
            .build();

        if obj.imp().flow_grid.get().is_none() {
            let flow_grid = FlowGrid::new(200, 16, 16);
            obj.set_flow_grid(flow_grid.clone());
            let container = obj.imp().container.get();
            container.append(&flow_grid.clone());
            obj.imp()
                .mobile
                .add_setter(&flow_grid, "minimum_size", &Value::from(150));
        }

        obj
    }

    pub fn should_hide_header(&self, should_hide: bool) {
        self.imp().header.set_visible(!should_hide);
    }
}

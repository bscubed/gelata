//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{Cell, RefCell};
use std::path::PathBuf;

use adw::subclass::prelude::*;
use glib::{clone, Properties};
use gtk::prelude::*;
use gtk::{gdk, gio, glib, CompositeTemplate};

use crate::data::DataManager;
use crate::models::enums::ImageType;
use crate::prelude::ClientExt;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/person.ui")]
    #[properties(wrapper_type = super::Person)]
    pub struct Person {
        // Template widgets
        #[template_child]
        pub(super) avatar: TemplateChild<adw::Avatar>,
        #[template_child]
        pub(super) name: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) subtitle: TemplateChild<gtk::Label>,
        #[template_child]
        pub container: TemplateChild<gtk::Box>,

        // Properties
        #[property(get, set)]
        pub(super) size: Cell<i32>,
        #[property(get, set)]
        pub(super) person_name: RefCell<String>,
        #[property(get, set)]
        pub(super) item_id: RefCell<String>,
        #[property(get, set)]
        pub(super) description: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Person {
        const NAME: &'static str = "Person";
        type Type = super::Person;
        type ParentType = gtk::Button;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Person {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            obj.bind_property::<gtk::Widget>("person-name", self.name.as_ref(), "label")
                .sync_create()
                .build();
            obj.bind_property::<gtk::Widget>("person-name", self.avatar.as_ref(), "text")
                .sync_create()
                .build();
            obj.bind_property::<gtk::Widget>("size", self.avatar.as_ref(), "size")
                .sync_create()
                .build();
            obj.bind_property::<gtk::Widget>("description", self.subtitle.as_ref(), "label")
                .sync_create()
                .build();
            obj.connect_description_notify(|obj| {
                obj.imp()
                    .subtitle
                    .set_visible(!obj.description().is_empty());
            });
        }
    }
    impl WidgetImpl for Person {
        fn root(&self) {
            self.parent_root();

            if self.obj().item_id() != String::default() {
                let item_id = self.obj().item_id();
                let image_type = ImageType::Primary;
                let image_path = DataManager::get_image_path(item_id.clone(), image_type.clone());

                if image_path.exists() {
                    let texture = gdk::Texture::from_file(&gio::File::for_path(image_path));
                    if let Ok(texture) = texture {
                        self.avatar.set_custom_image(Some(&texture));
                    }
                } else {
                    if let Some(client) = self.obj().client() {
                        let request_manager = client
                            .get_image(
                                item_id.clone(),
                                Some(image_type),
                                Some("400".to_string()),
                                None,
                            )
                            .unwrap();

                        request_manager.connect_success(
                            clone!(@weak self as this => move |_, image_path| {
                                let texture =
                                    gdk::Texture::from_file(&gio::File::for_path(PathBuf::from(image_path)));
                                if let Ok(texture) = texture {
                                    this.avatar.set_custom_image(Some(&texture));
                                }
                            }),
                        );
                        request_manager.fetch_image(image_path.clone());
                    }
                }
            }
        }
    }
    impl ButtonImpl for Person {}
}

glib::wrapper! {
    pub struct Person(ObjectSubclass<imp::Person>)
    @extends gtk::Button, gtk::Widget,
    @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl Person {
    pub fn new(name: String, size: Option<i32>, item_id: Option<String>) -> Self {
        glib::Object::builder()
            .property("person-name", name)
            .property("size", size.unwrap_or(96))
            .property("item-id", item_id.unwrap_or_default())
            .build()
    }

    pub fn remove_margin(&self) {
        let container = self.imp().container.get();
        container.set_margin_top(0);
        container.set_margin_bottom(0);
        container.set_margin_start(0);
        container.set_margin_end(0);
    }
}

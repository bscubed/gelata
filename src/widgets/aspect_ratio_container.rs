//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::Cell;

use glib::Properties;
use gtk::glib::{self};
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    #[derive(Default, Debug, Properties)]
    #[properties(wrapper_type = super::AspectRatioContainer)]
    pub struct AspectRatioContainer {
        #[property(get, set)]
        pub aspect_ratio: Cell<f32>,
        #[property(get, set)]
        pub max_width: Cell<i32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AspectRatioContainer {
        const NAME: &'static str = "AspectRatioContainer";
        type Type = super::AspectRatioContainer;
        type ParentType = gtk::Widget;
    }

    #[glib::derived_properties]
    impl ObjectImpl for AspectRatioContainer {
        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }
    }

    impl WidgetImpl for AspectRatioContainer {
        fn request_mode(&self) -> gtk::SizeRequestMode {
            gtk::SizeRequestMode::HeightForWidth
        }

        fn size_allocate(&self, width: i32, height: i32, baseline: i32) {
            self.obj()
                .first_child()
                .unwrap()
                .allocate(width, height, baseline, None);
        }

        fn measure(&self, orientation: gtk::Orientation, for_size: i32) -> (i32, i32, i32, i32) {
            let mut min_size = for_size;
            let mut nat_size = for_size;
            let mut min_baseline = -1;
            let mut nat_baseline = -1;

            match orientation {
                // If horizontal, calculate width based on minimum width of child
                gtk::Orientation::Horizontal => {
                    if for_size <= 0 {
                        let child = self.obj().first_child().unwrap();
                        (min_size, nat_size, min_baseline, nat_baseline) =
                            child.measure(orientation, for_size);
                    }
                },
                // If measuring vertical or otherwise, calculate height based on aspect ratio
                _ => {
                    // If we're not given a width, measure based off minimum width of child
                    if for_size <= 0 {
                        let child = self.obj().first_child().unwrap();
                        let (min_width, _, _, _) =
                            child.measure(gtk::Orientation::Horizontal, for_size);

                        min_size = (min_width as f32 * self.aspect_ratio.get()) as i32;
                        nat_size = min_size;
                    } else {
                        // In some cases, like when this widget is the child of a flexible layout (like
                        // a GtkScrolledWindow or AdwCarousel), we need to limit the width of this widget,
                        // otherwise measurements get out of control the layout blows up.
                        let max_width = self.max_width.get();
                        if max_width > 0 {
                            min_size = (for_size.min(self.max_width.get()) as f32
                                * self.aspect_ratio.get())
                                as i32;
                        } else {
                            min_size = (for_size as f32 * self.aspect_ratio.get()) as i32;
                        }
                        nat_size = min_size;
                    }
                },
            }

            // Return the calculated measurements
            (min_size, nat_size, min_baseline, nat_baseline)
        }
    }
}

glib::wrapper! {
    pub struct AspectRatioContainer(ObjectSubclass<imp::AspectRatioContainer>)
        @extends gtk::Widget,
        @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Accessible;
}

impl AspectRatioContainer {
    pub fn new(aspect_ratio: f32) -> Self {
        glib::Object::builder()
            .property("aspect-ratio", aspect_ratio)
            .build()
    }

    pub fn add_child<W: IsA<gtk::Widget>>(&self, widget: &W) {
        widget.set_parent(self);
    }
}

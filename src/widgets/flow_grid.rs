//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{Cell, RefCell};

use glib::Properties;
use gtk::glib::{self};
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    #[derive(Default, Debug, Properties)]
    #[properties(wrapper_type = super::FlowGrid)]
    pub struct FlowGrid {
        #[property(get, set)]
        pub minimum_size: Cell<i32>,
        #[property(get, set)]
        pub column_spacing: Cell<i32>,
        #[property(get, set)]
        pub row_spacing: Cell<i32>,
        #[property(get, set)]
        pub justify: RefCell<Option<String>>,
    }

    impl FlowGrid {
        pub fn visible_children(&self) -> Vec<gtk::Widget> {
            let mut children: Vec<gtk::Widget> = Vec::new();
            let mut optional = self.obj().upcast_ref::<gtk::Widget>().first_child();
            while let Some(child) = optional {
                if child.should_layout() {
                    children.push(child.clone());
                }
                optional = child.next_sibling();
            }
            children
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FlowGrid {
        const NAME: &'static str = "FlowGrid";
        type Type = super::FlowGrid;
        type ParentType = gtk::Widget;
    }

    #[glib::derived_properties]
    impl ObjectImpl for FlowGrid {
        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }
    }

    impl WidgetImpl for FlowGrid {
        fn request_mode(&self) -> gtk::SizeRequestMode {
            gtk::SizeRequestMode::HeightForWidth
        }

        fn measure(&self, orientation: gtk::Orientation, for_size: i32) -> (i32, i32, i32, i32) {
            let mut min_size = 0;
            let mut nat_size = 0;
            let mut min_baseline = -1;
            let mut nat_baseline = -1;

            if for_size > 0 {
                let children = self.visible_children();

                // Calculate the maximum amount of columns that can fit in a given width
                // with the width of each column being as close to the requested minimum
                // as possible
                let num_columns = ((for_size + self.column_spacing.get())
                    / (self.minimum_size.get() + self.column_spacing.get()))
                .max(1)
                .min(children.len() as i32);
                // Get the size of the columns based on the minimum column size while
                // making sure it doesn't become smaller than the minimum size specified
                let column_width = if children.len() as i32 == num_columns {
                    self.minimum_size.get() as f32
                } else {
                    ((for_size - (self.column_spacing.get() * (num_columns - 1))) as f32
                        / num_columns as f32)
                        .max(self.minimum_size.get() as f32)
                };

                let num_rows = (children.len() as f32 / num_columns as f32).ceil() as i32;
                let spacing_amount = self.row_spacing.get() * (num_rows - 1);

                // This vector tracks the maximum heights of views in each row so that each
                // row can vary its height depending on its largest element
                let mut min_row_heights = vec![0i32; num_rows as usize];
                let mut nat_row_heights = vec![0i32; num_rows as usize];

                for (index, child) in children.iter().enumerate() {
                    let (child_min, child_nat, child_min_baseline, child_nat_baseline) =
                        child.measure(orientation, column_width as i32);
                    let row = (index as i32 / num_columns) as usize;
                    // Update the maximum row heights if necessary
                    min_row_heights[row] = min_row_heights[row].max(child_min);
                    nat_row_heights[row] = nat_row_heights[row].max(child_nat);

                    if child_min_baseline > -1 {
                        min_baseline = min_baseline.max(child_min_baseline);
                    }
                    if child_nat_baseline > -1 {
                        nat_baseline = nat_baseline.max(child_nat_baseline);
                    }
                }

                min_size = min_row_heights.iter().sum::<i32>() + spacing_amount;
                nat_size = nat_row_heights.iter().sum::<i32>() + spacing_amount;
            }

            (min_size, nat_size, min_baseline, nat_baseline)
        }

        fn size_allocate(&self, width: i32, _height: i32, _baseline: i32) {
            // Get visible children
            let children = self.visible_children();
            let num_children = children.len() as i32;

            // Calculate the maximum amount of columns that can fit in a given width
            let num_columns = ((width + self.column_spacing.get())
                / (self.minimum_size.get() + self.column_spacing.get()))
            .max(1)
            .min(num_children);
            // Determine the size of the columns
            let column_width = if num_children == num_columns {
                self.minimum_size.get() as f32
            } else {
                ((width - self.column_spacing.get() * (num_columns - 1)) as f32
                    / num_columns as f32)
                    .max(self.minimum_size.get() as f32)
            };

            // Calculate the number of rows
            let num_rows = (num_children as f32 / num_columns as f32).ceil() as i32;
            // Track the maximum heights of views in each row
            let mut row_heights = vec![0i32; num_rows as usize];

            // Determine the maximum row height
            for (index, child) in children.iter().enumerate() {
                let row = (index as i32 / num_columns) as usize;
                let (child_min, _, _, _) =
                    child.measure(gtk::Orientation::Vertical, column_width as i32);
                row_heights[row] = row_heights[row].max(child_min);
            }

            // Allocate size to each child
            for (index, child) in children.iter().enumerate() {
                let index = index as i32;

                let column = index % num_columns;
                let row = index / num_columns;

                // Define x_offset, which represents the horizontal offset for child widgets.
                let x_offset = match (row + 1 != num_rows, num_children % num_columns) {
                    // When the row is the last one, calculate the width of the row and the
                    // empty space to the right of it.
                    (false, mut num_children_in_row) => {
                        // If there is only one row, we position as if it was the last row
                        if num_children_in_row == 0 {
                            num_children_in_row = num_columns;
                        }
                        // Width of the row is the sum of widths of all children in the row
                        // plus the sum of spacings between them.
                        let row_width = (column_width * num_children_in_row as f32)
                            + (self.column_spacing.get() * (num_children_in_row - 1)) as f32;
                        // Empty space is the total width minus the row width.
                        let empty_space = width as f32 - row_width;
                        // Depending on the justify property of the container,
                        // the offset is calculated differently.
                        match self.obj().justify().unwrap_or_default().as_str() {
                            // If justify is "center", the offset is half of the empty space.
                            "center" => (empty_space / 2.0) as i32,
                            // If justify is "end", the offset is equal to the empty space.
                            "end" => empty_space as i32,
                            // For all other cases (including the default "start"), there is no offset.
                            _ => 0,
                        }
                    },
                    // For all rows except the last one, there is no offset.
                    _ => 0,
                };
                let y_offset = (0..row)
                    .map(|row_index| row_heights[row_index as usize])
                    .sum::<i32>();
                let x = x_offset
                    + (column as f32 * column_width) as i32
                    + column * self.column_spacing.get();
                let y = y_offset + row * self.row_spacing.get();

                let child_width = column_width as i32;
                let child_height = row_heights[row as usize];

                child.size_allocate(&gtk::Allocation::new(x, y, child_width, child_height), -1);
            }
        }
    }

    impl BuildableImpl for FlowGrid {}
}

glib::wrapper! {
    pub struct FlowGrid(ObjectSubclass<imp::FlowGrid>)
        @extends gtk::Widget,
        @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Accessible;
}

impl FlowGrid {
    pub fn new(minimum_size: i32, column_spacing: i32, row_spacing: i32) -> Self {
        glib::Object::builder()
            .property("minimum-size", minimum_size)
            .property("column-spacing", column_spacing)
            .property("row-spacing", row_spacing)
            .build()
    }

    pub fn add_child<W: IsA<gtk::Widget>>(&self, widget: &W) {
        widget.set_parent(self);
    }
}

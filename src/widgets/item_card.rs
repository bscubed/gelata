//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::borrow::Borrow;
use std::cell::{OnceCell, RefCell};

use adw::subclass::prelude::*;
use adw::NavigationPage;
use glib::{clone, Properties};
use gtk::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::data::DataManager;
use crate::models::enums::{BaseItemKind, ImageType};
use crate::models::objects::BaseItemDto;
use crate::prelude::{ClientExt, NavigationExt};
use crate::widgets::{AspectRatioContainer, CollectionPage, ItemPage};

mod imp {
    use super::*;

    #[derive(Properties, Default, Debug, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/item_card.ui")]
    #[properties(wrapper_type = super::ItemCard)]
    pub struct ItemCard {
        // Properties
        #[property(get, set)]
        pub(super) title: RefCell<String>,
        #[property(get, set)]
        pub(super) subtitle: RefCell<Option<String>>,
        #[property(get, set)]
        pub(super) item_id: RefCell<String>,
        #[property(get, set)]
        pub(super) image_id: RefCell<Option<String>>,

        // Template widgets
        #[template_child]
        pub(super) title_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) subtitle_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) image: TemplateChild<gtk::Picture>,
        #[template_child]
        pub(super) aspect_ratio: TemplateChild<AspectRatioContainer>,
        #[template_child]
        pub(super) container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) stack: TemplateChild<gtk::Stack>,

        pub(super) item: OnceCell<BaseItemDto>,
    }

    #[gtk::template_callbacks]
    impl ItemCard {
        #[template_callback]
        fn clicked(&self, _: glib::Value) {
            let child: gtk::Widget = match self.item.get().unwrap().type_field.clone().unwrap() {
                BaseItemKind::AggregateFolder
                | BaseItemKind::BasePluginFolder
                | BaseItemKind::BoxSet
                | BaseItemKind::ChannelFolderItem
                | BaseItemKind::CollectionFolder
                | BaseItemKind::Folder
                | BaseItemKind::Genre
                | BaseItemKind::ManualPlaylistsFolder
                | BaseItemKind::MusicAlbum
                | BaseItemKind::MusicArtist
                | BaseItemKind::MusicGenre
                | BaseItemKind::PhotoAlbum
                | BaseItemKind::Playlist
                | BaseItemKind::PlaylistsFolder
                | BaseItemKind::Season
                | BaseItemKind::Series
                | BaseItemKind::UserRootFolder => {
                    CollectionPage::new(self.item.get().unwrap()).into()
                },
                _ => ItemPage::new(
                    self.title.borrow().clone(),
                    self.item.get().unwrap().id.clone(),
                )
                .into(),
            };
            // Sets the tag of the page to {item_id}-player so we can find it later to reopen
            let page_tag = &self.item.get().unwrap().id.clone();
            let navigation_page =
                NavigationPage::with_tag(&child, &*self.title.borrow(), &page_tag).clone();
            if let Some(navigation) = self.obj().navigation() {
                navigation.push(&navigation_page);
            }
        }

        pub fn setup_ui(&self) {
            self.title_label.set_label(&self.title.borrow());
            if let Some(subtitle) = self.subtitle.borrow().as_ref() {
                self.subtitle_label.set_label(&subtitle);
            } else {
                self.subtitle_label.set_visible(false);
            }

            let stack = self.stack.get();
            let picture = self.image.get();

            if self.image_id.borrow().is_some() {
                let item_id = self.item_id.borrow();
                let image_type = ImageType::Primary;
                let image_path = DataManager::get_image_path(item_id.clone(), image_type.clone());

                if image_path.exists() {
                    picture
                        .set_filename(Some(image_path.as_os_str().to_str().unwrap().to_string()));
                    stack.set_visible_child_name("image");
                } else {
                    if let Some(client) = self.obj().client() {
                        let request_manager = client
                            .get_image(
                                item_id.clone(),
                                Some(image_type),
                                Some("400".to_string()),
                                None,
                            )
                            .unwrap();

                        request_manager.connect_success(
                            clone!(@weak self as this => move |_, image_path| {
                                picture.set_filename(Some(image_path));
                                stack.set_visible_child_name("image");
                            }),
                        );
                        request_manager.connect_error(
                            clone!(@weak self as this => move |_, error_str| {
                                error!("{}", error_str);
                            }),
                        );
                        request_manager.fetch_image(image_path.clone());
                    }
                }
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemCard {
        const NAME: &'static str = "ItemCard";
        type Type = super::ItemCard;
        type ParentType = gtk::Button;

        fn class_init(klass: &mut Self::Class) {
            AspectRatioContainer::ensure_type();

            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ItemCard {
        fn constructed(&self) {
            self.obj().connect_width_request_notify(move |this| {
                let mut width = this.width_request();
                if width > 0 {
                    let container = this.imp().container.borrow();
                    if container.is_visible() {
                        width = width - container.margin_start() - container.margin_end();
                    }
                    this.imp().aspect_ratio.set_max_width(width);
                }
            });
        }
    }

    impl WidgetImpl for ItemCard {
        fn root(&self) {
            self.parent_root();

            self.setup_ui();
        }
    }
    impl ButtonImpl for ItemCard {}
}

glib::wrapper! {
    pub struct ItemCard(ObjectSubclass<imp::ItemCard>)
        @extends gtk::Button, gtk::Widget,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl ItemCard {
    pub fn new(item: BaseItemDto) -> Self {
        let item_clone = item.clone();

        let obj: Self = glib::Object::builder()
            .property("title", item.name.unwrap())
            .property("item-id", item.id)
            .property("image-id", item.image_tags.unwrap().primary)
            .property("subtitle", item_clone.formatted_year())
            .build();

        obj.imp().item.set(item_clone).unwrap();

        obj
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::OnceCell;

use adw::subclass::prelude::*;
use glib::clone;
use gtk::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::data::DataManager;
use crate::models::{Session, User};
use crate::prelude::{ClientExt, WindowExt};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/user_login_page.ui")]
    pub struct UserLoginPage {
        // Template widgets
        #[template_child]
        pub password_entry: TemplateChild<gtk::PasswordEntry>,
        #[template_child]
        pub finish_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub avatar: TemplateChild<adw::Avatar>,
        #[template_child]
        pub name: TemplateChild<gtk::Label>,

        pub(super) user: OnceCell<User>,
    }

    #[gtk::template_callbacks]
    impl UserLoginPage {
        #[template_callback]
        fn finish_clicked(&self, _: &gtk::Button) {
            let password = self.password_entry.get().text().to_string();
            if let Some(client) = self.obj().client() {
                let request_manager = client
                    .authenticate_by_user_id(self.user.get().unwrap().id.clone(), password)
                    .expect("Client not configured correctly");
                request_manager.connect_success(clone!(@weak self as this, @weak client => move |_, response| {
                    let mut session: Session = serde_json::from_str::<Session>(response.as_str()).unwrap();
                    session.hostname = Some(client.hostname());
                    session.port = Some(client.port());
                    session.use_https = Some(client.use_https());
                    client.set_token(session.access_token.clone());
                    client.set_user(session.user.clone());
                    client.set_session_info(session.session_info.clone());

                    if let Ok(path) = DataManager::encode_data(&session, "session.bin") {
                        info!("Successfully saved session data to {}", path);
                    } else {
                        error!("Failed to save session data to data directory");
                    }

                    // We can safely unwrap here since the window must exist in order for
                    // the client to make requests in the first place.
                    this.obj().window().unwrap().imp().setup_ui();
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UserLoginPage {
        const NAME: &'static str = "UserLoginPage";
        type Type = super::UserLoginPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for UserLoginPage {}
    impl WidgetImpl for UserLoginPage {}
    impl BoxImpl for UserLoginPage {}
    impl OrientableImpl for UserLoginPage {}
}

glib::wrapper! {
    pub struct UserLoginPage(ObjectSubclass<imp::UserLoginPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl UserLoginPage {
    pub fn new(user: User) -> Self {
        let obj: Self = glib::Object::new();

        obj.imp()
            .user
            .set(user.clone())
            .expect("User shouldn't be set on a new object");

        obj.imp().avatar.get().set_text(Some(&user.name));
        obj.imp().name.get().set_text(&user.name);

        obj
    }
}

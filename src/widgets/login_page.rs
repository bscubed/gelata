//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::OnceCell;

use adw::subclass::prelude::*;
use glib::{clone, Properties};
use gtk::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::application::GelataApplication;
use crate::data::DataManager;
use crate::models::Session;
use crate::prelude::{ClientExt, WindowExt};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/login_page.ui")]
    #[properties(wrapper_type = super::LoginPage)]
    pub struct LoginPage {
        // Template widgets
        #[template_child]
        pub username_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub password_entry: TemplateChild<gtk::PasswordEntry>,
        #[template_child]
        pub finish_button: TemplateChild<gtk::Button>,

        // Properties
        #[property(get, set)]
        pub application: OnceCell<GelataApplication>,
    }

    #[gtk::template_callbacks]
    impl LoginPage {
        #[template_callback]
        fn finish_clicked(&self, _: &gtk::Button) {
            let username = self.username_entry.get().text().to_string();
            let password = self.password_entry.get().text().to_string();
            if let Some(client) = self.obj().client() {
                let request_manager = client
                    .authenticate_by_name(username, password)
                    .expect("Client not configured correctly");
                request_manager.connect_success(clone!(@weak self as this, @weak client => move |_, response| {
                    let mut session: Session = serde_json::from_str::<Session>(response.as_str()).unwrap();
                    session.hostname = Some(client.hostname());
                    session.port = Some(client.port());
                    session.use_https = Some(client.use_https());
                    client.set_token(session.access_token.clone());
                    client.set_user(session.user.clone());
                    client.set_session_info(session.session_info.clone());

                    if let Ok(path) = DataManager::encode_data(&session, "session.bin") {
                        info!("Successfully saved session data to {}", path);
                    } else {
                        error!("Failed to save session data to data directory");
                    }

                    // We can safely unwrap here since the window must exist in order for
                    // the client to make requests in the first place.
                    this.obj().window().unwrap().imp().setup_ui();
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }

        #[template_callback]
        fn username_changed(&self, _: &gtk::Entry) {
            self.update_finish_button();
        }

        fn update_finish_button(&self) {
            if !self.username_entry.text().is_empty() {
                self.finish_button.set_opacity(1.0);
            } else {
                self.finish_button.set_opacity(0.0);
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LoginPage {
        const NAME: &'static str = "LoginPage";
        type Type = super::LoginPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for LoginPage {}
    impl WidgetImpl for LoginPage {}
    impl BoxImpl for LoginPage {}
    impl OrientableImpl for LoginPage {}
}

glib::wrapper! {
    pub struct LoginPage(ObjectSubclass<imp::LoginPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl LoginPage {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

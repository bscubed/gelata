//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;
use gtk::{glib, CompositeTemplate};

use crate::models::enums::{BaseItemKind, CollectionType};
use crate::models::objects::{BaseItemDto, ListResponse};
use crate::prelude::*;
use crate::widgets::{Banner, CollectionPage, HorizontalScroller, ItemCard};

mod imp {
    use super::*;

    #[derive(Default, Debug, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/home_page.ui")]
    pub struct HomePage {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub stack: TemplateChild<adw::ViewStack>,
        #[template_child]
        pub navigation_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        pub carousel: TemplateChild<adw::Carousel>,
        #[template_child]
        pub left: TemplateChild<gtk::Button>,
        #[template_child]
        pub right: TemplateChild<gtk::Button>,
        #[template_child]
        pub container: TemplateChild<gtk::Box>,
    }

    #[gtk::template_callbacks]
    impl HomePage {
        #[template_callback]
        fn page_stack_changed(&self, _spec: glib::ParamSpec) {
            // Update window title to reflect currently selected page
            if let Some(child) = self.stack.visible_child() {
                let stack_page = self.stack.page(&child);

                if let Some(navigation_page) = self.navigation_view.visible_page() {
                    navigation_page.set_title(&stack_page.title().unwrap_or_default())
                }
            }

            // Notify server which item we're viewing
            if let Some(name) = self.stack.visible_child_name() {
                if let Some(client) = self.obj().client() {
                    let request_manager = client.report_viewing_item(name.to_string()).unwrap();
                    request_manager.connect_error(
                        clone!(@weak self as this => move |_, error_str| {
                            error!("{}", error_str)
                        }),
                    );
                    request_manager.fetch();
                }
            }
        }

        #[template_callback]
        fn navigation_page_changed(&self, _spec: glib::ParamSpec) {
            if let Some(tag) = self
                .navigation_view
                .visible_page()
                .and_then(|page| page.tag())
            {
                if let Some(client) = self.obj().client() {
                    let request_manager = client.report_viewing_item(tag.to_string()).unwrap();
                    request_manager.connect_error(
                        clone!(@weak self as this => move |_, error_str| {
                            error!("{}", error_str)
                        }),
                    );
                    request_manager.fetch();
                }
            }
        }

        pub(super) fn setup_ui(&self) {
            self.load_libraries();
            self.load_banner();
            self.load_continue_watching();
            self.load_latest_media();
        }

        pub fn load_libraries(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_views().unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    let media_folders_response: ListResponse<BaseItemDto> = match serde_json::from_str::<ListResponse<BaseItemDto>>(response.as_str()) {
                        Ok(media_folders_value) => media_folders_value,
                        Err(e) => {
                            error!("Failed parsing media folders response: {:?}", e);
                            return
                        },
                    };
                    let media_folders = media_folders_response.items;

                    for media_folder in &media_folders {
                        let collection_icon = match media_folder.collection_type.clone().unwrap() {
                            CollectionType::Movies => {
                                "movies-symbolic"
                            },
                            CollectionType::TvShows => {
                                "tv-shows-symbolic"
                            },
                            CollectionType::Music => {
                                "music-symbolic"
                            },
                            CollectionType::MusicVideos => {
                                "music-videos-symbolic"
                            },
                            CollectionType::HomeVideos => {
                                "home-videos-symbolic"
                            },
                            CollectionType::BoxSets => {
                                "boxsets-symbolic"
                            },
                            CollectionType::Folders => {
                                "folders-symbolic"
                            },
                            CollectionType::Playlists => {
                                "playlists-symbolic"
                            },
                        };

                        let child = CollectionPage::new(media_folder);
                        // Having the child's header visible would conflict with the home page's header
                        child.should_hide_header(true);
                        this.stack.add_titled_with_icon(&child, Some(media_folder.id.as_str()), media_folder.name.clone().unwrap().as_str(), collection_icon);
                    }
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));

                request_manager.fetch();
            }
        }

        fn load_banner(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_banner_suggestions().unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    let suggestions: Vec<BaseItemDto> = serde_json::from_str::<ListResponse<BaseItemDto>>(response.as_str()).unwrap_or_default().items;

                    for suggestion in suggestions {
                        let banner = Banner::new(suggestion.id);
                        banner.set_height_request(300);
                        this.carousel.append(&banner);
                    }
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));

                request_manager.fetch();
            }
        }

        fn add_item_category(&self, title: String, items: Vec<BaseItemDto>) {
            if items.len() > 0 {
                let scroller = HorizontalScroller::new(title);

                for item in items {
                    let card = ItemCard::new(item);
                    card.set_width_request(200);
                    card.set_halign(gtk::Align::Start);

                    scroller.add_child(&card);
                }

                self.container.append(&scroller);
            }
        }

        fn load_continue_watching(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_continue_watching().unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    let items: Vec<BaseItemDto> = serde_json::from_str::<ListResponse<BaseItemDto>>(response.as_str()).unwrap_or_default().items;

                    this.add_item_category("Continue Watching".into(), items);
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }

        fn load_latest_media(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_latest_media().unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    let items: Vec<BaseItemDto> = serde_json::from_str::<Vec<BaseItemDto>>(response.as_str()).unwrap();
                    let mut hash_map: HashMap<BaseItemKind, Vec<BaseItemDto>> = HashMap::new();

                    // Creates a multi-value per key hash map with items organized by type
                    for item in items.iter() {
                        hash_map.entry(item.clone().type_field.unwrap()).or_default().push(item.clone());
                    }

                    for (key, value) in hash_map {
                            let title = "Recently Added ".to_string() + key.plural_str();

                            this.add_item_category(title, value);
                    }
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str)
                }));
                request_manager.fetch();
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HomePage {
        const NAME: &'static str = "HomePage";
        type Type = super::HomePage;
        type ParentType = adw::BreakpointBin;

        fn class_init(klass: &mut Self::Class) {
            HorizontalScroller::ensure_type();

            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl WidgetImpl for HomePage {
        fn root(&self) {
            self.parent_root();

            self.setup_ui();
        }
    }

    impl ObjectImpl for HomePage {}
    impl BreakpointBinImpl for HomePage {}
    impl OrientableImpl for HomePage {}
}

glib::wrapper! {
    pub struct HomePage(ObjectSubclass<imp::HomePage>)
        @extends gtk::Widget, adw::BreakpointBin,
        @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Accessible;
}

impl HomePage {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

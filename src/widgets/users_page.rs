//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use adw::subclass::prelude::*;
use adw::NavigationPage;
use glib::clone;
use gtk::prelude::*;
use gtk::{glib, CompositeTemplate};

use super::UserLoginPage;
use crate::models::User;
use crate::prelude::NavigationExt;
use crate::widgets::{FlowGrid, LoginPage, Person};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/users_page.ui")]
    pub struct UsersPage {
        // Template widgets
        #[template_child]
        pub(super) flow_grid: TemplateChild<FlowGrid>,
    }

    #[gtk::template_callbacks]
    impl UsersPage {
        #[template_callback]
        fn manual_login_clicked(&self, _: &gtk::Button) {
            // Open login view
            let login_page = LoginPage::new();
            let login_navigation_page = adw::NavigationPage::new(&login_page, "User Login").clone();
            if let Some(navigation) = self.obj().navigation() {
                navigation.push(&login_navigation_page);
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UsersPage {
        const NAME: &'static str = "UsersPage";
        type Type = super::UsersPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for UsersPage {}
    impl WidgetImpl for UsersPage {}
    impl BoxImpl for UsersPage {}
    impl OrientableImpl for UsersPage {}
}

glib::wrapper! {
    pub struct UsersPage(ObjectSubclass<imp::UsersPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl UsersPage {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn add_users(&self, users: Vec<User>) {
        for user in users.iter() {
            self.add_user(user);
        }
    }

    pub fn add_user(&self, user: &User) {
        let user_avatar = Person::new(user.clone().name, None, None);
        user_avatar.connect_clicked(clone!(@weak self as this, @strong user => move |_| {
            let user_login_page = UserLoginPage::new(user.clone());
            let user_login_page = NavigationPage::new(&user_login_page, "User Login").clone();

            if let Some(navigation) = this.navigation() {
                navigation.push(&user_login_page);
            }
        }));
        user_avatar.remove_margin();
        self.imp().flow_grid.add_child(&user_avatar);
    }
}

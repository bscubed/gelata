//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use adw::subclass::prelude::*;
use adw::NavigationPage;
use glib::clone;
use gtk::glib::ParamSpec;
use gtk::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::models::User;
use crate::prelude::ClientExt;
use crate::widgets::{LoginPage, UsersPage};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/onboard_page.ui")]
    pub struct OnboardPage {
        // Template widgets
        #[template_child]
        pub navigation_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        pub carousel: TemplateChild<adw::Carousel>,
        #[template_child]
        pub prev_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub next_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub server_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub server_status: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub test_connection_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub address_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub port_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub use_https_switch: TemplateChild<gtk::Switch>,
    }

    #[gtk::template_callbacks]
    impl OnboardPage {
        #[template_callback]
        fn next_clicked(&self, _: &gtk::Button) {
            let current_position = self.carousel.position();
            if ((current_position + 1.0) as u32) < self.carousel.n_pages() {
                let next_page = self.carousel.nth_page((current_position + 1.0) as u32);
                self.carousel.scroll_to(&next_page, true);
            } else if ((current_position + 1.0) as u32) == self.carousel.n_pages() {
                self.update_client();
                if let Some(client) = self.obj().client() {
                    match client.public_users() {
                        Some(request_manager) => {
                            request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                                let public_users: Vec<User> = match serde_json::from_str::<Vec<User>>(response.as_str()) {
                                    Ok(public_users_value) => public_users_value,
                                    Err(err) => {
                                        error!("{:?}", err);
                                        [].to_vec()
                                    },
                                };

                                if public_users.is_empty() {
                                    // Open login view
                                    let login_page = LoginPage::new();
                                    let login_navigation_page = NavigationPage::new(&login_page, "User Login").clone();
                                    this.navigation_view.push(&login_navigation_page);
                                } else {
                                    // Open select user view
                                    let users_page = UsersPage::new();
                                    users_page.add_users(public_users);
                                    let users_navigation_page = NavigationPage::new(&users_page, "User Select").clone();
                                    this.navigation_view.push(&users_navigation_page);
                                }
                            }));
                            request_manager.connect_error(
                                clone!(@weak self as this => move |_, error_str| {
                                    error!("{}", error_str)
                                }),
                            );

                            request_manager.fetch();
                        },
                        None => {
                            error!("Error: Client not configured")
                        },
                    };
                }
            }
        }

        #[template_callback]
        fn previous_clicked(&self, _: &gtk::Button) {
            let current_position = self.carousel.position();
            if current_position as u32 > 0 {
                let previous_page = self.carousel.nth_page((current_position - 1.0) as u32);
                self.carousel.scroll_to(&previous_page, true);
            }
        }

        #[template_callback]
        fn carousel_position_changed(&self, _spec: ParamSpec) {
            let position = self.carousel.position();
            if position < 1.0 {
                let opacity = position % 1.0;
                self.prev_button.set_opacity(opacity);
                self.prev_button.set_visible(opacity >= 0.0);
            };
            if position > (self.carousel.n_pages() - 1).into() {
                let opacity = (self.carousel.n_pages() - 1) as f64 - position;
                self.next_button.set_opacity(opacity);
                self.next_button.set_visible(opacity >= 0.0);
            }
        }

        #[template_callback]
        fn reset_error_status(&self, entry: &gtk::Entry) {
            entry.remove_css_class("error");
        }

        #[template_callback]
        fn test_connection_clicked(&self, _: &gtk::Button) {
            self.server_stack.set_visible_child_name("spinner");

            self.update_client();
            if let Some(client) = self.obj().client() {
                match client.ping() {
                    Some(request_manager) => {
                        request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                            if response == "\"Jellyfin Server\"" {
                                this.server_stack.set_visible_child_name("status");
                                this.server_status.set_title("You're good to go");
                                this.server_status.set_description(Some(format!("Connection to {} was successful", Self::get_entry_value(&this.address_entry)).as_str()));
                                this.server_status.set_icon_name(Some("test-pass-symbolic"))
                            }
                        }));
                        request_manager.connect_error(clone!(@weak self as this => move |_, _error_str| {
                            this.server_stack.set_visible_child_name("status");
                            this.server_status.set_title("Something went wrong");
                            this.server_status.set_description(Some("Review your server configuration and try again"));
                            this.server_status.set_icon_name(Some("sad-computer-symbolic"))
                        }));

                        request_manager.fetch();
                    },
                    None => {
                        self.server_stack.set_visible_child_name("status");
                        self.server_status.set_title("Something went wrong");
                        self.server_status.set_description(Some(
                            "Review your server configuration and try again",
                        ));
                        self.server_status
                            .set_icon_name(Some("sad-computer-symbolic"))
                    },
                };
            }
        }

        fn update_client(&self) {
            let port = match Self::get_entry_value(&self.port_entry).parse::<u16>() {
                Ok(parsed_port) => parsed_port,
                Err(error) => {
                    error!("Failed to parse port input: {:?}", error);
                    self.server_stack.set_visible_child_name("status");
                    let _ = &self.port_entry.add_css_class("error");
                    return;
                },
            };
            let address = Self::get_entry_value(&self.address_entry);
            let use_https = self.use_https_switch.state();

            if let Some(client) = self.obj().client() {
                client.set_hostname(address.to_string());
                client.set_port(port as u32);
                client.set_use_https(use_https);
            }
        }

        fn get_entry_value(entry: &gtk::Entry) -> glib::GString {
            let mut val = entry.text();
            if val.is_empty() {
                val = match entry.placeholder_text() {
                    Some(text) => text,
                    None => val,
                }
            }
            val
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for OnboardPage {
        const NAME: &'static str = "OnboardPage";
        type Type = super::OnboardPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for OnboardPage {}
    impl WidgetImpl for OnboardPage {
        fn root(&self) {
            self.parent_root();
        }
    }
    impl BoxImpl for OnboardPage {}
    impl OrientableImpl for OnboardPage {}
}

glib::wrapper! {
    pub struct OnboardPage(ObjectSubclass<imp::OnboardPage>)
    @extends gtk::Widget, gtk::Box,
    @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Orientable;
}

impl OnboardPage {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{Cell, RefCell};

use glib::Properties;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate, Widget};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/horizontal_scroller.ui")]
    #[properties(wrapper_type = super::HorizontalScroller)]
    pub struct HorizontalScroller {
        #[template_child]
        pub title_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub overlay: TemplateChild<gtk::Overlay>,
        #[template_child]
        pub carousel: TemplateChild<adw::Carousel>,
        #[template_child]
        pub left: TemplateChild<gtk::Button>,
        #[template_child]
        pub right: TemplateChild<gtk::Button>,

        #[property(get, set)]
        pub title: RefCell<String>,
        #[property(get, set)]
        pub carousel_index: Cell<i32>,
        #[property(get, set)]
        pub items_per_view: Cell<i32>,
    }

    #[gtk::template_callbacks]
    impl HorizontalScroller {
        #[template_callback]
        fn left_clicked(&self, _: &gtk::Button) {
            let new_index = std::cmp::max(self.carousel_index.get() - self.items_per_view.get(), 0);
            if self.carousel_index.get() != new_index {
                self.carousel_index.set(new_index);
                let new_page = self.carousel.nth_page(new_index as u32);
                self.carousel.scroll_to(&new_page, true);
            }
        }

        #[template_callback]
        fn right_clicked(&self, _: &gtk::Button) {
            let new_index = std::cmp::min(
                self.carousel_index.get() + self.items_per_view.get(),
                self.carousel.n_pages() as i32 - 1,
            );
            if self.carousel_index.get() != new_index {
                self.carousel_index.set(new_index);
                let new_page = self.carousel.nth_page(new_index as u32);
                self.carousel.scroll_to(&new_page, true);
            }
        }

        #[template_callback]
        fn page_changed(&self, _spec: glib::ParamSpec) {
            self.update_buttons();
        }

        fn update_buttons(&self) {
            let page = self.carousel.position();

            // It might be tempting to replace `set_sensitive` and `set_opacity` with just `set_visible`
            // however if we do that, the signal for the overlay stops firing, which this function is
            // dependent on.
            let left_shown = page > 0.0;
            self.left.set_sensitive(left_shown);
            self.left.set_opacity(left_shown as u8 as f64);

            let right_shown =
                page < (self.carousel.n_pages() as i32 - self.items_per_view.get()) as f64;
            self.right.set_sensitive(right_shown);
            self.right.set_opacity(right_shown as u8 as f64);
        }

        #[template_callback]
        fn size_changed(&self, _: gtk::Widget, allocation: gtk::Allocation) -> bool {
            let carousel = self.carousel.get();
            if carousel.n_pages() > 0 {
                let new_width = allocation.width();
                let page_width = carousel.allocated_width();
                let carousel_spacing = carousel.spacing() as i32;
                let items =
                    (new_width + carousel_spacing) as f32 / (page_width + carousel_spacing) as f32;
                self.items_per_view.set(items as i32);
                self.update_buttons();
            }
            true
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HorizontalScroller {
        const NAME: &'static str = "HorizontalScroller";
        type Type = super::HorizontalScroller;
        type ParentType = gtk::Widget;
        type Interfaces = (gtk::Buildable,);

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();

            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for HorizontalScroller {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();

            obj.connect_title_notify(|obj| {
                obj.imp().title_label.set_visible(!obj.title().is_empty());
            });

            obj.bind_property::<Widget>("title", self.title_label.as_ref(), "label")
                .sync_create()
                .build();
        }

        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }
    }

    impl WidgetImpl for HorizontalScroller {}

    impl BuildableImpl for HorizontalScroller {
        fn add_child(&self, builder: &gtk::Builder, child: &glib::Object, type_: Option<&str>) {
            let buildable = self.obj();

            if !self.carousel.is_bound() {
                self.parent_add_child(builder, child, type_);
            } else {
                buildable.add_child(child.downcast_ref::<gtk::Widget>().unwrap());
            };
        }
    }
}

glib::wrapper! {
    pub struct HorizontalScroller(ObjectSubclass<imp::HorizontalScroller>)
        @extends gtk::Widget,
        @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Accessible;
}

impl HorizontalScroller {
    pub fn new(title: String) -> Self {
        glib::Object::builder().property("title", title).build()
    }

    pub fn add_child<T: IsA<gtk::Widget>>(&self, widget: &T) {
        let imp = self.imp();
        imp.carousel.append(widget);
        imp.overlay.set_visible(true);
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{OnceCell, RefCell};
use std::sync::OnceLock;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::subclass::Signal;
use glib::{clone, Properties, SignalFlags};
use gtk::{glib, CompositeTemplate};

use crate::application::GelataApplication;
use crate::data::DataManager;
use crate::models::enums::ImageType;
use crate::prelude::*;

mod imp {
    use super::*;

    #[derive(Properties, Default, Debug, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/banner.ui")]
    #[properties(wrapper_type = super::Banner)]
    // Banner? I hardly know her!
    pub struct Banner {
        // Properties
        #[property(get, set)]
        pub(super) navigation: RefCell<adw::NavigationView>,
        #[property(get, set)]
        pub(super) application: OnceCell<GelataApplication>,
        #[property(get, set)]
        pub(super) item_id: RefCell<String>,

        // Template widgets
        #[template_child]
        pub(super) backdrop: TemplateChild<gtk::Picture>,
        #[template_child]
        pub(super) logo: TemplateChild<gtk::Picture>,

        pub(super) request_queue: RefCell<Vec<String>>,
    }

    #[gtk::template_callbacks]
    impl Banner {
        pub(super) fn setup_ui(&self) {
            self.request_queue
                .replace(["backdrop".into(), "logo".into()].to_vec());

            self.setup_backdrop();
            self.setup_logo();
        }

        fn setup_backdrop(&self) {
            let item_id = self.obj().item_id();
            let image_type = ImageType::Backdrop;
            let image_path = DataManager::get_image_path(item_id.clone(), image_type.clone());

            if image_path.exists() {
                self.backdrop
                    .set_filename(Some(image_path.as_os_str().to_str().unwrap().to_string()));

                self.request_complete("backdrop".into());
            } else {
                if let Some(client) = self.obj().client() {
                    let request_manager = client
                        .get_image(
                            item_id.clone(),
                            Some(image_type),
                            Some("1500".to_string()),
                            None,
                        )
                        .unwrap();

                    request_manager.connect_success(
                        clone!(@weak self as this => move |_, image_path| {
                            this.backdrop.set_filename(Some(image_path));

                            this.request_complete("backdrop".into());
                        }),
                    );
                    request_manager.connect_error(clone!(@weak self as this => move |_, _| {
                        // No backdrop for this item_id
                        this.request_complete("backdrop".into());
                    }));
                    request_manager.fetch_image(image_path.clone());
                }
            }
        }

        fn setup_logo(&self) {
            let item_id = self.obj().item_id();
            let image_type = ImageType::Logo;
            let image_path = DataManager::get_image_path(item_id.clone(), image_type.clone());

            if image_path.exists() {
                self.logo
                    .set_filename(Some(image_path.as_os_str().to_str().unwrap().to_string()));

                self.request_complete("logo".into());
            } else {
                if let Some(client) = self.obj().client() {
                    let request_manager = client
                        .get_image(
                            item_id.clone(),
                            Some(image_type),
                            Some("500".to_string()),
                            None,
                        )
                        .unwrap();

                    request_manager.connect_success(
                        clone!(@weak self as this => move |_, image_path| {
                            this.logo.set_filename(Some(image_path));

                            this.request_complete("logo".into());
                        }),
                    );
                    request_manager.connect_error(clone!(@weak self as this => move |_, _| {
                        // No logo for this item_id
                        this.request_complete("logo".into());
                    }));
                    request_manager.fetch_image(image_path.clone());
                }
            }
        }

        fn request_complete(&self, request: String) {
            let mut request_queue = self.request_queue.borrow_mut();
            if let Some(i) = request_queue.iter().position(|x| *x == request) {
                request_queue.remove(i);

                if request_queue.is_empty() {
                    // Emit a signal when loading is complete
                    self.obj().emit_by_name::<()>("loaded", &[]);
                }
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Banner {
        const NAME: &'static str = "Banner";
        type Type = super::Banner;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Banner {
        fn signals() -> &'static [Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS
                .get_or_init(|| vec![Signal::builder("loaded").flags(SignalFlags::ACTION).build()])
        }
    }

    impl WidgetImpl for Banner {
        fn root(&self) {
            self.parent_root();

            self.setup_ui();
        }
    }
    impl BoxImpl for Banner {}
    impl OrientableImpl for Banner {}
}

glib::wrapper! {
    pub struct Banner(ObjectSubclass<imp::Banner>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl Banner {
    pub fn new(item_id: String) -> Self {
        glib::Object::builder().property("item-id", item_id).build()
    }

    pub fn connect_loaded<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("loaded", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            f(&obj);
            None
        })
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    #[derive(Default, Debug)]
    pub struct ConstraintContainer {}

    #[glib::object_subclass]
    impl ObjectSubclass for ConstraintContainer {
        const NAME: &'static str = "ConstraintContainer";
        type Type = super::ConstraintContainer;
        type ParentType = gtk::Widget;
    }

    impl ObjectImpl for ConstraintContainer {
        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }
    }

    impl WidgetImpl for ConstraintContainer {}
}

glib::wrapper! {
    pub struct ConstraintContainer(ObjectSubclass<imp::ConstraintContainer>)
        @extends gtk::Widget,
        @implements gtk::ConstraintTarget, gtk::Buildable, gtk::Accessible;
}

impl ConstraintContainer {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{OnceCell, RefCell};

use adw::prelude::*;
use adw::subclass::prelude::*;
use adw::NavigationPage;
use glib::{clone, Properties, Value};
use gtk::{glib, CompositeTemplate};

use crate::data::DataManager;
use crate::models::enums::{ImageType, MediaStreamType};
use crate::models::objects::{BaseItemDto, ListResponse, PlaybackInfo};
use crate::player::Player;
use crate::prelude::*;
use crate::utils::ticks_to_str;
use crate::widgets::{Banner, CoverBanner, HorizontalScroller, ItemCard, Person};

mod imp {
    use super::*;

    #[derive(Properties, Default, Debug, CompositeTemplate)]
    #[template(resource = "/dev/bscubed/Gelata/gtk/item_page.ui")]
    #[properties(wrapper_type = super::ItemPage)]
    pub struct ItemPage {
        // Properties
        #[property(get, set)]
        pub(super) title: RefCell<String>,
        #[property(get, set)]
        pub(super) item_id: RefCell<String>,

        // Template widgets
        #[template_child]
        pub(super) loader_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub(super) title_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) play_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) trailer_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) watched_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) favorite_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) language_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) more_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) banner: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) cover: TemplateChild<gtk::Stack>,
        #[template_child]
        pub(super) image: TemplateChild<gtk::Picture>,
        #[template_child]
        pub(super) title_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) main_content: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) tagline: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) description: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) release_year: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) release_year_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) runtime: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) runtime_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) rating: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) rating_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) community_rating: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) community_rating_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) critic_rating: TemplateChild<gtk::Label>,
        #[template_child]
        pub(super) critic_rating_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) cast: TemplateChild<HorizontalScroller>,
        #[template_child]
        pub(super) mobile: TemplateChild<adw::Breakpoint>,
        #[template_child]
        pub(super) similar_items: TemplateChild<HorizontalScroller>,
        #[template_child]
        pub(super) audio_list: TemplateChild<gtk::StringList>,
        #[template_child]
        pub(super) subtitle_list: TemplateChild<gtk::StringList>,
        #[template_child]
        pub(super) audio_dropdown_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) subtitle_dropdown_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub(super) audio_dropdown: TemplateChild<gtk::DropDown>,
        #[template_child]
        pub(super) subtitle_dropdown: TemplateChild<gtk::DropDown>,
        #[template_child]
        pub(super) language_dialog: TemplateChild<adw::Dialog>,

        pub(super) item: OnceCell<BaseItemDto>,
        pub(super) request_queue: RefCell<Vec<String>>,
    }

    #[gtk::template_callbacks]
    impl ItemPage {
        #[template_callback]
        fn play_clicked(&self, _: &gtk::Button) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_playback_info(&self.item_id.borrow()).unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    if let Ok(playback_info) = serde_json::from_str::<PlaybackInfo>(&response) {
                        let media_sources = playback_info.media_sources.unwrap();
                        let default_source = match media_sources.first() {
                            Some(source) => source,
                            None => {
                                error!("No media source found for {}", this.item.get().unwrap().name.clone().unwrap());
                                return;
                            },
                        };
                        let media_streams = default_source.media_streams.clone().unwrap();

                        let e_tag = default_source.e_tag.clone().unwrap();
                        let play_session_id = playback_info.play_session_id.clone().unwrap();
                        let media_source_id = default_source.id.clone().unwrap();

                        let audio_track = if this.audio_dropdown_container.is_visible() {
                            Some(this.audio_dropdown.selected() as i32)
                        } else {
                            None
                        };

                        let subtitle_track = if this.subtitle_dropdown_container.is_visible() {
                            Some(this.subtitle_dropdown.selected() as i32)
                        } else {
                            None
                        };

                        let player = Player::new(this.item_id.borrow().clone(), e_tag, play_session_id, media_source_id, media_streams, default_source.run_time_ticks.clone().unwrap(), audio_track, subtitle_track);
                        let player_page = NavigationPage::new(&player, &*this.title.borrow());
                        if let Some(navigation) = this.obj().navigation() {
                            navigation.push(&player_page);
                        }
                    }
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str);
                }));
                request_manager.fetch();
            }
        }

        #[template_callback]
        fn language_clicked(&self, _: &gtk::Button) {
            if let Some(window) = self.obj().window() {
                self.language_dialog.present(&window);
            }
        }

        #[template_callback]
        fn setup_dropdown_label(&self, object: &glib::Object) {
            if let Some(list_item) = object.downcast_ref::<gtk::ListItem>() {
                let label = gtk::Label::new(None);
                label.set_ellipsize(gtk::pango::EllipsizeMode::End);
                label.set_single_line_mode(true);
                label.set_lines(1);

                list_item.set_child(Some(&label));
            }
        }

        #[template_callback]
        fn bind_dropdown_label(&self, object: &glib::Object) {
            if let Some(list_item) = object.downcast_ref::<gtk::ListItem>() {
                if let Some(text) = list_item.item().and_downcast::<gtk::StringObject>() {
                    let label = list_item.child().and_downcast::<gtk::Label>().unwrap();
                    label.set_label(text.string().as_str());
                }
            }
        }

        pub fn setup_ui(&self) {
            self.request_queue
                .replace(["item".into(), "cover".into(), "banner".into()].to_vec());

            self.title_label.set_label(&self.title.borrow());

            self.setup_banner();
            self.setup_item();
            self.setup_related_items();
            self.setup_cover();
        }

        fn setup_banner(&self) {
            let banner = Banner::new(self.obj().item_id());

            banner.connect_loaded(clone!(@weak self as this => move |_| {
                this.request_complete("banner".into());
            }));

            self.banner.append(&banner);
        }

        fn setup_item(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_item(&self.item_id.borrow()).unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    if let Ok(item) = serde_json::from_str::<BaseItemDto>(&response) {
                        this.setup_title_info(&item);
                        this.setup_main_view(&item);
                        this.setup_tracks(&item);

                        this.item.set(item.clone()).unwrap();

                        this.request_complete("item".into());
                    }
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str);
                }));
                request_manager.fetch();
            }
        }

        fn setup_related_items(&self) {
            if let Some(client) = self.obj().client() {
                let request_manager = client.get_similar_items(&self.item_id.borrow()).unwrap();

                request_manager.connect_success(clone!(@weak self as this => move |_, response| {
                    if let Ok(result) = serde_json::from_str::<ListResponse<BaseItemDto>>(response.as_str()) {
                        for item in result.items {
                            let card = ItemCard::new(item);
                            card.set_width_request(200);
                            card.set_halign(gtk::Align::Start);

                            // Let breakpoint handle size transitions
                            this.mobile
                                .add_setter(&card, "width-request", &Value::from(150));

                            this.similar_items.add_child(&card);
                            this.similar_items.set_visible(true);
                        }
                    }
                }));
                request_manager.connect_error(clone!(@weak self as this => move |_, error_str| {
                    error!("{}", error_str);
                }));
                request_manager.fetch();
            }
        }

        fn setup_title_info(&self, item: &BaseItemDto) {
            if let Some(release_year) = &item.production_year {
                self.release_year
                    .set_label(&(*release_year as i32).to_string());
                self.release_year_container.set_visible(true);
            }

            if let Some(runtime) = &item.run_time_ticks {
                self.runtime.set_label(&ticks_to_str(*runtime));
                self.runtime_container.set_visible(true);
            }

            if let Some(rating) = &item.official_rating {
                self.rating.set_label(rating.as_str());
                self.rating_container.set_visible(true);
            }

            if let Some(community_rating) = &item.community_rating {
                self.community_rating
                    .set_label(&format!("{:.2}", community_rating));
                self.community_rating_container.set_visible(true);
            }

            if let Some(critic_rating) = &item.critic_rating {
                self.critic_rating
                    .set_label(&format!("{:.0}%", critic_rating));
                self.critic_rating_container.set_visible(true);
            }
        }

        fn setup_main_view(&self, item: &BaseItemDto) {
            let taglines = item.clone().taglines.unwrap_or_default();
            if let Some(tagline) = taglines.first() {
                self.tagline.set_label(tagline.as_str());
                self.tagline.set_visible(true);
            }

            if let Some(overview) = item.clone().overview {
                self.description.set_label(overview.as_str());
                self.description.set_visible(true);
            }

            if let Some(cast) = &item.people {
                for person in cast {
                    let person_avatar =
                        Person::new(person.clone().name.unwrap(), Some(150), person.clone().id);
                    person_avatar.add_css_class("card");
                    if let Some(role) = person.clone().role {
                        person_avatar.set_description(role);
                    }

                    // Let breakpoint handle size transitions
                    self.mobile
                        .add_setter(&person_avatar, "size", &Value::from(100));

                    self.cast.add_child(&person_avatar);
                    self.cast.set_visible(true);
                }
            }

            if let Some(item_type) = &item.type_field {
                self.similar_items
                    .set_title(format!("Related {}", item_type.plural_str()));
            }
        }

        fn setup_tracks(&self, item: &BaseItemDto) {
            let media_sources = item.media_sources.clone().unwrap();
            let default_source = match media_sources.first() {
                Some(source) => source,
                None => {
                    error!("No media source found for {}", item.name.clone().unwrap());
                    return;
                },
            };
            let media_streams = default_source.media_streams.clone().unwrap();

            let audio_streams: Vec<String> = media_streams
                .iter()
                .filter(|val| val.type_field == Some(MediaStreamType::Audio))
                .map(|val| val.display_title.clone().unwrap_or_default())
                .collect();
            let subtitle_streams: Vec<String> = media_streams
                .iter()
                .filter(|val| val.type_field == Some(MediaStreamType::Subtitle))
                .map(|val| val.display_title.clone().unwrap_or_default())
                .collect();

            for stream in audio_streams {
                self.audio_list.append(stream.as_str());
                self.audio_dropdown_container.set_visible(true);
            }
            for stream in subtitle_streams {
                self.subtitle_list.append(stream.as_str());
                self.subtitle_dropdown_container.set_visible(true);
            }
        }

        fn setup_cover(&self) {
            let stack = self.cover.get();
            let picture = self.image.get();
            let item_id = self.item_id.borrow();
            let image_type = ImageType::Primary;
            let image_path = DataManager::get_image_path(item_id.clone(), image_type.clone());

            if image_path.exists() {
                picture.set_filename(Some(image_path.as_os_str().to_str().unwrap().to_string()));
                stack.set_visible_child_name("image");

                self.request_complete("cover".into());
            } else {
                if let Some(client) = self.obj().client() {
                    let request_manager = client
                        .get_image(
                            item_id.clone(),
                            Some(image_type),
                            Some("400".to_string()),
                            None,
                        )
                        .unwrap();

                    request_manager.connect_success(
                        clone!(@weak self as this => move |_, image_path| {
                            picture.set_filename(Some(image_path));
                            stack.set_visible_child_name("image");

                            this.request_complete("cover".into());
                        }),
                    );
                    request_manager.connect_error(clone!(@weak self as this => move |_, _| {
                        // No cover image for this item
                        this.request_complete("cover".into());
                    }));
                    request_manager.fetch_image(image_path.clone());
                }
            }
        }

        fn request_complete(&self, request: String) {
            let mut request_queue = self.request_queue.borrow_mut();
            if let Some(i) = request_queue.iter().position(|x| *x == request) {
                request_queue.remove(i);

                if request_queue.is_empty() {
                    self.loader_stack.set_visible_child_name("content");
                }
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemPage {
        const NAME: &'static str = "ItemPage";
        type Type = super::ItemPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            // Register `CoverBanner`
            CoverBanner::ensure_type();

            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ItemPage {}
    impl WidgetImpl for ItemPage {
        fn root(&self) {
            self.parent_root();

            self.setup_ui();
        }
    }
    impl BoxImpl for ItemPage {}
    impl OrientableImpl for ItemPage {}
}

glib::wrapper! {
    pub struct ItemPage(ObjectSubclass<imp::ItemPage>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Orientable;
}

impl ItemPage {
    pub fn new(title: String, item_id: String) -> Self {
        glib::Object::builder()
            .property("title", title)
            .property("item-id", item_id)
            .build()
    }
}

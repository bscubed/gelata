//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::OnceLock;
use std::time::Duration;

use glib::subclass::prelude::*;
use glib::subclass::Signal;
use glib::{subclass, Properties};
use gtk::prelude::*;
use json::object;
use once_cell::sync::Lazy;
use reqwest::header::HeaderMap;
use reqwest::{ClientBuilder, Method};

use super::socket_manager::SocketManager;
use super::RequestManager;
use crate::config::{APP_NAME, VERSION};
use crate::models::enums::ImageType;
use crate::models::{SessionInfo, User};
use crate::CAPABILITIES;

pub static USER_AGENT: Lazy<String> = Lazy::new(|| format!("{}/{}", APP_NAME, VERSION));

pub static HEADERS: Lazy<HeaderMap> = Lazy::new(|| {
    let mut map = HeaderMap::new();
    map.insert("Content-Type", "application/json".parse().unwrap());
    map.insert("Accept", "application/json".parse().unwrap());
    map.insert("Accept-Charset", "utf-8".parse().unwrap());
    map.insert("X-Application", USER_AGENT.parse().unwrap());
    map.insert("User-Agent", USER_AGENT.parse().unwrap());
    map
});

pub static HTTP_CLIENT: Lazy<reqwest::Client> = Lazy::new(|| {
    ClientBuilder::new()
        .timeout(Duration::from_secs(15))
        .default_headers(HEADERS.to_owned())
        .build()
        .unwrap()
});

pub static BLOCKING_HTTP_CLIENT: Lazy<reqwest::blocking::Client> = Lazy::new(|| {
    reqwest::blocking::ClientBuilder::new()
        .timeout(Duration::from_secs(15))
        .default_headers(HEADERS.to_owned())
        .build()
        .unwrap()
});

mod imp {
    use super::*;

    #[derive(Properties, Default)]
    #[properties(wrapper_type = super::Client)]
    pub struct Client {
        pub(super) requests: RefCell<Vec<RequestManager>>,
        #[property(get, set)]
        pub(crate) token: RefCell<String>,
        #[property(get, set)]
        pub(crate) hostname: RefCell<String>,
        #[property(get, set)]
        pub(crate) port: RefCell<u32>,
        #[property(get, set)]
        pub(crate) use_https: RefCell<bool>,
        pub(crate) user: RefCell<User>,
        pub(crate) session_info: RefCell<SessionInfo>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Client {
        const NAME: &'static str = "Client";
        type Type = super::Client;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Client {
        fn signals() -> &'static [subclass::Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| {
                vec![
                    Signal::builder("success")
                        .param_types([String::static_type()])
                        .build(),
                    Signal::builder("error")
                        .param_types([String::static_type()])
                        .build(),
                ]
            })
        }
    }
}

glib::wrapper! {
    pub struct Client(ObjectSubclass<imp::Client>);
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

impl Client {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }

    fn build_url(&self, endpoint: &str) -> Option<String> {
        let use_https = self.use_https();
        let scheme = if use_https { "https" } else { "http" };
        let hostname = self.hostname();
        let port = self.port();

        Some(format!("{}://{}:{}{}", scheme, hostname, port, endpoint))
    }

    pub fn get_socket(&self) -> SocketManager {
        let protocol = if self.imp().use_https.borrow().to_owned() {
            "wss"
        } else {
            "ws"
        };
        SocketManager::new(
            protocol.to_string(),
            self.imp().hostname.borrow().clone(),
            self.imp().port.borrow().to_string(),
            self.imp().token.borrow().clone(),
        )
    }

    // This function retrieves a request manager, which is used to manage network requests.
    // It takes in the HTTP method, endpoint, optional query parameters, optional body,
    // and a boolean indicating whether the request should be authenticated.
    fn retrieve(
        &self,
        method: Method,                           // HTTP method (GET, POST, etc.)
        endpoint: &str,                           // The API endpoint
        queries: Option<HashMap<String, String>>, // Optional query parameters
        body: Option<String>,                     // Optional request body
        authenticated: bool,                      // Whether the request should be authenticated
    ) -> Option<RequestManager> {
        // Build the URL using the provided endpoint.
        // If the URL cannot be built, emit an error and return None.
        let url = match self.build_url(endpoint) {
            Some(url_value) => url_value,
            None => {
                self.emit_by_name::<()>("error", &[&"URL values not initiated"]);
                return None;
            },
        };

        // Create a new request manager with the built URL.
        let request_manager = RequestManager::new(url);
        let request_manager_clone = request_manager.clone();

        // Set the HTTP method for the request.
        request_manager_clone.set_method(method);

        // If query parameters are provided, set them for the request.
        if queries.clone().is_some() {
            request_manager_clone.set_queries(queries.as_ref().unwrap());
        }

        // If a body is provided, set it for the request.
        if body.clone().is_some() {
            request_manager_clone.set_body(body.unwrap());
        }

        // If the request should be authenticated and the current instance is authenticated,
        // set the token for the request.
        if authenticated && self.is_authenticated() {
            request_manager_clone.set_token(self.token());
        }

        // Add the request manager to the list of requests.
        let mut requests = self.imp().requests.borrow_mut();
        requests.push(request_manager);

        // Return the cloned request manager.
        Some(request_manager_clone)
    }

    // This function is used to send a blocking HTTP request and retrieve the response.
    // It takes the HTTP method, endpoint, optional query parameters, optional body, and a boolean to indicate if the request should be authenticated.
    // It returns a Result with the HTTP response or an error message.
    fn retrieve_blocking(
        &self,
        method: Method,                           // The HTTP method for the request.
        endpoint: &str,                           // The endpoint for the request.
        queries: Option<HashMap<String, String>>, // Optional query parameters for the request.
        body: Option<String>,                     // Optional body for the request.
        authenticated: bool, // Boolean to indicate if the request should be authenticated.
    ) -> Result<reqwest::blocking::Response, String> {
        // Build the URL for the request.
        let url = match self.build_url(endpoint) {
            Some(url_value) => url_value,
            None => {
                // Emit an error event if the URL values are not initiated.
                self.emit_by_name::<()>("error", &[&"URL values not initiated"]);
                // Return an error if the URL values are not initiated.
                return Err("URL values not initiated".to_string());
            },
        };

        // Create the request.
        let mut request = BLOCKING_HTTP_CLIENT.request(method, url);

        // Add the query parameters to the request if they exist.
        if queries.clone().is_some() {
            request = request.query(&queries.unwrap());
        }

        // Add the body to the request if it exists.
        if body.clone().is_some() {
            request = request.body(body.unwrap());
        }

        // Add the authentication token to the request if it should be authenticated and the client is authenticated.
        if authenticated && self.is_authenticated() {
            request = request.header("X-MediaBrowser-Token", self.token());
        }

        // Send the request and return the response or an error.
        match request.send() {
            Ok(response) => Ok(response),
            Err(e) => Err(e.to_string()),
        }
    }

    pub fn is_authenticated(&self) -> bool {
        !self.imp().token.borrow().is_empty()
    }

    pub fn is_configured(&self) -> bool {
        if !self.is_authenticated() {
            return false;
        }
        if let Ok(response) = self.get_blocking("/Users/Me", None, true) {
            if let Ok(text) = response.text() {
                if let Ok(user) = serde_json::from_str::<User>(&text) {
                    info!("Successfully authenticated user {}", user.name);
                    return true;
                }
            }
        }
        false
    }

    pub fn set_user(&self, user: User) {
        let _ = self.imp().user.replace(user);
    }

    pub fn set_session_info(&self, session_info: SessionInfo) {
        let _ = self.imp().session_info.replace(session_info);
    }

    pub fn get(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        authenticated: bool,
    ) -> Option<RequestManager> {
        self.retrieve(Method::GET, endpoint, queries, None, authenticated)
    }

    pub fn get_blocking(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        authenticated: bool,
    ) -> Result<reqwest::blocking::Response, String> {
        self.retrieve_blocking(Method::GET, endpoint, queries, None, authenticated)
    }

    pub fn post(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        body: Option<String>,
        authenticated: bool,
    ) -> Option<RequestManager> {
        self.retrieve(Method::POST, endpoint, queries, body, authenticated)
    }

    pub fn post_blocking(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        body: Option<String>,
        authenticated: bool,
    ) -> Result<reqwest::blocking::Response, String> {
        self.retrieve_blocking(Method::POST, endpoint, queries, body, authenticated)
    }

    pub fn put(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        body: Option<String>,
        authenticated: bool,
    ) -> Option<RequestManager> {
        self.retrieve(Method::PUT, endpoint, queries, body, authenticated)
    }

    pub fn put_blocking(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        body: Option<String>,
        authenticated: bool,
    ) -> Result<reqwest::blocking::Response, String> {
        self.retrieve_blocking(Method::PUT, endpoint, queries, body, authenticated)
    }

    pub fn delete(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        authenticated: bool,
    ) -> Option<RequestManager> {
        self.retrieve(Method::DELETE, endpoint, queries, None, authenticated)
    }

    pub fn delete_blocking(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        authenticated: bool,
    ) -> Result<reqwest::blocking::Response, String> {
        self.retrieve_blocking(Method::DELETE, endpoint, queries, None, authenticated)
    }

    pub fn head(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        authenticated: bool,
    ) -> Option<RequestManager> {
        self.retrieve(Method::HEAD, endpoint, queries, None, authenticated)
    }

    pub fn head_blocking(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        authenticated: bool,
    ) -> Result<reqwest::blocking::Response, String> {
        self.retrieve_blocking(Method::HEAD, endpoint, queries, None, authenticated)
    }

    pub fn patch(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        body: Option<String>,
        authenticated: bool,
    ) -> Option<RequestManager> {
        self.retrieve(Method::PATCH, endpoint, queries, body, authenticated)
    }

    pub fn patch_blocking(
        &self,
        endpoint: &str,
        queries: Option<HashMap<String, String>>,
        body: Option<String>,
        authenticated: bool,
    ) -> Result<reqwest::blocking::Response, String> {
        self.retrieve_blocking(Method::PATCH, endpoint, queries, body, authenticated)
    }

    pub fn get_image(
        &self,
        item_id: String,
        image_type: Option<ImageType>,
        width: Option<String>,
        height: Option<String>,
    ) -> Option<RequestManager> {
        let mut queries: HashMap<String, String> = HashMap::new();
        width.map(|w| queries.insert("maxWidth".to_string(), w));
        height.map(|h| queries.insert("maxHeight".to_string(), h));

        let image_type_unwrapped = image_type.unwrap_or_else(|| ImageType::Primary);
        self.get(
            &format!(
                "/Items/{item_id}/Images/{}",
                image_type_unwrapped.to_string()
            ),
            Some(queries),
            true,
        )
    }

    // API methods

    pub fn ping(&self) -> Option<RequestManager> {
        self.get("/System/Ping", None, false)
    }

    pub fn public_users(&self) -> Option<RequestManager> {
        self.get("/Users/Public", None, false)
    }

    pub fn authenticate_by_user_id(&self, user_id: String, pw: String) -> Option<RequestManager> {
        self.post(
            &format!("/Users/{user_id}/Authenticate"),
            Some(HashMap::from([("pw".to_string(), pw)])),
            None,
            false,
        )
    }

    pub fn authenticate_by_name(&self, username: String, pw: String) -> Option<RequestManager> {
        let body = object! {
            Username: username,
            Pw: pw,
        };
        self.post("/Users/AuthenticateByName", None, Some(body.dump()), false)
    }

    pub fn get_views(&self) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        let queries = HashMap::from([("includeHidden".to_string(), "false".to_string())]);
        self.get(
            format!("/Users/{user_id}/Views").as_str(),
            Some(queries),
            true,
        )
    }

    pub fn get_items(&self, parent_id: &str) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        let queries = HashMap::from([
            ("parentId".to_string(), parent_id.to_string()),
            ("sortOrder".to_string(), "Ascending".to_string()),
            ("sortBy".to_string(), "SortName".to_string()),
        ]);
        self.get(
            format!("/Users/{user_id}/Items").as_str(),
            Some(queries),
            true,
        )
    }

    pub fn get_item(&self, item_id: &str) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        self.get(
            format!("/Users/{user_id}/Items/{item_id}").as_str(),
            None,
            true,
        )
    }

    pub fn update_capabilities(&self) -> Option<RequestManager> {
        let body = CAPABILITIES.dump();
        self.post("/Sessions/Capabilities/Full", None, Some(body), true)
    }

    pub fn get_playback_info(&self, item_id: &str) -> Option<RequestManager> {
        let queries = HashMap::from([("userId".to_string(), self.imp().user.borrow().id.clone())]);
        self.get(
            &format!("/Items/{item_id}/PlaybackInfo"),
            Some(queries),
            true,
        )
    }

    pub fn get_video_stream_url(
        &self,
        item_id: &str,
        e_tag: &str,
        is_static: Option<bool>,
        play_session_id: Option<&str>,
        media_source_id: Option<&str>,
    ) -> String {
        let mut queries: HashMap<String, String> = HashMap::new();
        queries.insert("tag".to_string(), e_tag.to_string());
        is_static.map(|is_static| queries.insert("static".to_string(), is_static.to_string()));
        play_session_id.map(|play_session_id| {
            queries.insert("playSessionId".to_string(), play_session_id.to_string())
        });
        media_source_id.map(|media_source_id| {
            queries.insert("mediaSourceId".to_string(), media_source_id.to_string())
        });
        let endpoint = &format!("/Videos/{item_id}/stream");
        let mut url = self.build_url(endpoint).unwrap().to_string();
        for (key, value) in &queries {
            if url.contains('?') {
                url.push_str(&format!("&{}={}", key, value));
            } else {
                url.push_str(&format!("?{}={}", key, value));
            }
        }
        url.clone()
    }

    pub fn get_banner_suggestions(&self) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        let queries = HashMap::from([
            ("recursive".to_string(), "true".to_string()),
            ("imageTypes".to_string(), "Backdrop,Logo".to_string()),
            ("sortBy".to_string(), "Random".to_string()),
            ("limit".to_string(), "10".to_string()),
        ]);
        self.get(
            format!("/Users/{user_id}/Items").as_str(),
            Some(queries),
            true,
        )
    }

    pub fn get_continue_watching(&self) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        self.get(
            format!("/Users/{user_id}/Items/Resume").as_str(),
            None,
            true,
        )
    }

    pub fn get_latest_media(&self) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        self.get(
            format!("/Users/{user_id}/Items/Latest").as_str(),
            None,
            true,
        )
    }

    pub fn report_viewing_item(&self, item_id: String) -> Option<RequestManager> {
        let queries = HashMap::from([("itemId".to_string(), item_id)]);
        self.post("/Sessions/Viewing", Some(queries), None, true)
    }

    pub fn report_playing_item(&self, item_id: String) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        self.post(
            format!("/Users/{user_id}/PlayingItems/{item_id}").as_str(),
            None,
            None,
            true,
        )
    }

    pub fn report_stopped_playing(&self, item_id: String) -> Option<RequestManager> {
        let user_id = self.imp().user.borrow().id.clone();
        self.delete(
            format!("/Users/{user_id}/PlayingItems/{item_id}").as_str(),
            None,
            true,
        )
    }

    pub fn report_playback_progress(
        &self,
        item_id: String,
        position: i64,
        volume_level: i32,
        is_paused: bool,
    ) -> Option<RequestManager> {
        let queries = HashMap::from([
            ("positionTicks".to_string(), position.to_string()),
            ("volumeLevel".to_string(), volume_level.to_string()),
            ("isPaused".to_string(), is_paused.to_string()),
        ]);
        let user_id = self.imp().user.borrow().id.clone();
        self.post(
            format!("/Users/{user_id}/PlayingItems/{item_id}/Progress").as_str(),
            Some(queries),
            None,
            true,
        )
    }

    pub fn get_similar_items(&self, item_id: &str) -> Option<RequestManager> {
        let queries = HashMap::from([
            ("userId".to_string(), self.imp().user.borrow().id.clone()),
            ("limit".to_string(), 10.to_string()),
        ]);
        self.get(
            format!("/Items/{item_id}/Similar").as_str(),
            Some(queries),
            true,
        )
    }
}

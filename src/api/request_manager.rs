//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{OnceCell, RefCell};
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::OnceLock;

use glib::subclass::prelude::*;
use glib::subclass::Signal;
use glib::{clone, subclass, Properties};
use gtk::prelude::*;
use reqwest::{Method, Response};
use tokio::runtime::Runtime;

use super::client::HTTP_CLIENT;
use crate::data::DataManager;
use crate::{APP_NAME, VERSION};

mod imp {
    use super::*;

    #[derive(Properties, Default, Debug)]
    #[properties(wrapper_type = super::RequestManager)]
    pub struct RequestManager {
        #[property(get, set)]
        pub(super) status: RefCell<String>,
        #[property(get, set)]
        pub(super) url: OnceCell<String>,
        #[property(get, set)]
        pub(super) body: OnceCell<String>,
        #[property(get, set)]
        pub(super) token: OnceCell<String>,
        pub(super) queries: OnceCell<HashMap<String, String>>,
        pub(super) method: OnceCell<Method>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RequestManager {
        const NAME: &'static str = "RequestManager";
        type Type = super::RequestManager;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for RequestManager {
        fn signals() -> &'static [subclass::Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| {
                vec![
                    Signal::builder("success")
                        .param_types([String::static_type()])
                        .build(),
                    Signal::builder("error")
                        .param_types([String::static_type()])
                        .build(),
                ]
            })
        }
    }
}

glib::wrapper! {
    pub struct RequestManager(ObjectSubclass<imp::RequestManager>);
}

impl RequestManager {
    pub fn new(url: String) -> Self {
        glib::Object::builder()
            .property("status", "idle")
            .property("url", url)
            .build()
    }

    pub fn set_queries(&self, queries: &HashMap<String, String>) {
        let self_ = imp::RequestManager::from_obj(self);
        self_.queries.set(queries.clone()).unwrap();
    }

    pub fn set_method(&self, method: Method) {
        let self_ = imp::RequestManager::from_obj(self);
        self_.method.set(method).unwrap();
    }

    pub fn runtime() -> &'static Runtime {
        static RUNTIME: OnceLock<Runtime> = OnceLock::new();
        RUNTIME.get_or_init(|| Runtime::new().expect("Setting up tokio runtime needs to succeed."))
    }

    // This function is used to build an authorization header for a MediaBrowser client.
    // It uses the device id and token from the current instance of the class (self).
    // If the device id or token is not available, it will use an empty string instead.
    // The final output is a formatted string which includes the app name, device type (Linux),
    // device id, version, and token.
    pub fn build_authorization_header(&self) -> String {
        // Get the token. If it's not available, use an empty string.
        let token = self
            .imp()
            .token
            .get()
            .map_or(String::new(), |token| format!(", Token=\"{}\"", token));

        format!(
            "MediaBrowser Client=\"{}\", Device=\"Linux\", DeviceId=\"{}\", Version=\"{}\"{}",
            APP_NAME,
            machine_uid::get().unwrap(),
            VERSION,
            token
        )
    }

    pub fn fetch(&self) {
        self.fetch_action(false, None);
    }

    pub fn fetch_image(&self, image_path: PathBuf) {
        self.fetch_action(true, Some(image_path));
    }

    // This function fetches an action with the possibility of downloading an image.
    // The function takes two parameters:
    // - image: a boolean indicating whether an image is to be downloaded or not.
    // - image_path: an optional parameter specifying the path to save the image if one is to be downloaded.
    pub fn fetch_action(&self, image: bool, image_path: Option<PathBuf>) {
        // Get the URL from the instance.
        let url = self.url();

        // Get the request method. If it's not specified, log an error and emit an error signal, then return.
        let method = match self.imp().method.get() {
            Some(method_value) => method_value,
            None => {
                error!("No request method specified");
                self.emit_by_name::<()>("error", &[&"No request method specified"]);
                return;
            },
        };

        // Start building the request.
        let mut request = HTTP_CLIENT
            .request(method.clone(), url)
            .header("Authorization", self.build_authorization_header());

        // If a request body is specified, add it to the request.
        if let Some(body_value) = self.imp().body.get() {
            request = request.body(body_value.clone());
        }

        // If query parameters are specified, add them to the request.
        if let Some(queries_value) = self.imp().queries.get() {
            request = request.query(queries_value);
        }

        // If an image is to be downloaded, specify the content type as image/jpeg.
        if image {
            request = request.header("Content-Type", "image/jpeg");
        }

        // Create an asynchronous channel to handle the request.
        let (sender, receiver) = async_channel::bounded::<Result<Response, reqwest::Error>>(1);

        // Spawn a new task to send the request.
        Self::runtime().spawn(clone!(@strong sender => async move {
            let result = request.send().await;
            sender.send(result).await.expect("The channel needs to be open");
        }));

        // Spawn a new task to handle the response.
        glib::MainContext::default().spawn_local(clone!(@weak self as this => async move {
            while let Ok(result) = receiver.recv().await {
                match result {
                    Ok(result_value) => {
                        // If the request was successful, handle the response.
                        if result_value.status().is_success() {
                            if image {
                                // If an image was downloaded, save it to the specified path.
                                match result_value.bytes().await {
                                    Ok(image_data) => {
                                        let image_path = image_path.clone().unwrap();
                                        match DataManager::save_image(image_path.clone(), image_data) {
                                            Ok(_) => {
                                                this.set_status("loaded");
                                                this.emit_by_name::<()>("success", &[&image_path.as_os_str().to_str().unwrap().to_string()]);
                                            },
                                            Err(e) => {
                                                this.set_status("error");
                                                this.emit_by_name::<()>("error", &[&e.to_string()]);
                                            },
                                        }
                                    },
                                    Err(e) => {
                                        this.set_status("error");
                                        this.emit_by_name::<()>("error", &[&e.to_string()]);
                                    },
                                }
                            } else {
                                // If no image was downloaded, handle the text response.
                                match result_value.text().await {
                                    Ok(text_value) => {
                                        this.set_status("loaded");
                                        this.emit_by_name::<()>("success", &[&text_value]);
                                    },
                                    Err(e) => {
                                        this.set_status("error");
                                        this.emit_by_name::<()>("error", &[&e.to_string()]);
                                    },
                                }
                            }
                        } else {
                            // If the request was not successful, log an error and emit an error signal.
                            this.set_status("error");
                            this.emit_by_name::<()>("error", &[&format!("Error code recieved as response: {:?}", result_value)]);
                        }
                    }
                    Err(e) => {
                        // If an error occurred while handling the request, log it and emit an error signal.
                        this.set_status("error");
                        this.emit_by_name::<()>("error", &[&e.to_string()]);
                    }
                }
            }
        }));
    }

    pub fn connect_success<F: Fn(&Self, String) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("success", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            let response = values[1].get::<String>().unwrap();
            f(&obj, response);
            None
        })
    }

    pub fn connect_error<F: Fn(&Self, String) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("error", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            let error_str = values[1].get::<String>().unwrap();
            f(&obj, error_str);
            None
        })
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::borrow::Borrow;
use std::cell::{OnceCell, RefCell};
use std::time::Duration;

use glib::{clone, SourceId};

use super::SocketManager;
use crate::models::MessageType;

#[derive(Default, Debug)]
pub struct KeepAlive {
    timeout_id: RefCell<Option<SourceId>>,
    interval: Duration,
    socket_manager: OnceCell<SocketManager>,
}

impl KeepAlive {
    pub fn new(interval: Duration, socket_manager: SocketManager) -> Self {
        Self {
            timeout_id: RefCell::new(None),
            interval: interval.div_f32(2.0),
            socket_manager: OnceCell::from(socket_manager),
        }
    }

    pub fn stop(&self) {
        if let Some(timeout_id) = self.timeout_id.replace(None) {
            timeout_id.remove();
        }
    }

    pub fn start(&self) {
        let socket_manager = self.socket_manager.get().unwrap().clone();
        self.timeout_id.replace(Some(glib::timeout_add_local(
            *self.interval.borrow(),
            clone!(@strong socket_manager => move || {
                socket_manager.send(MessageType::KeepAlive, None);
                glib::ControlFlow::Continue
            }),
        )));
    }
}

//   Copyright (c) 2024 Brendan Szymanski
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::cell::{OnceCell, RefCell};
use std::sync::OnceLock;
use std::time::Duration;

use async_channel::Sender;
use async_tungstenite::gio::connect_async;
use async_tungstenite::tungstenite::Message;
use futures_util::{SinkExt, StreamExt};
use glib::subclass::prelude::*;
use glib::subclass::Signal;
use glib::{clone, subclass, Properties, SignalFlags};
use gtk::prelude::*;
use gtk_macros::spawn;
use json::{object, JsonValue};

use crate::api::KeepAlive;
use crate::models::{MessageType, SocketMessage};

mod imp {
    use super::*;

    #[derive(Properties, Default, Debug)]
    #[properties(wrapper_type = super::SocketManager)]
    pub struct SocketManager {
        #[property(get, set)]
        pub(super) protocol: OnceCell<String>,
        #[property(get, set)]
        pub(super) hostname: OnceCell<String>,
        #[property(get, set)]
        pub(super) port: OnceCell<String>,
        #[property(get, set)]
        pub(super) token: OnceCell<String>,

        pub(super) message_sender: OnceCell<Sender<Message>>,
        pub(super) keepalive: RefCell<Option<KeepAlive>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SocketManager {
        const NAME: &'static str = "SocketManager";
        type Type = super::SocketManager;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for SocketManager {
        fn signals() -> &'static [subclass::Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| {
                vec![
                    Signal::builder("message")
                        .param_types([String::static_type()])
                        .build(),
                    Signal::builder("ping")
                        .param_types([String::static_type()])
                        .build(),
                    Signal::builder("stop").flags(SignalFlags::ACTION).build(),
                    Signal::builder("error")
                        .param_types([String::static_type()])
                        .build(),
                ]
            })
        }
    }
}

glib::wrapper! {
    pub struct SocketManager(ObjectSubclass<imp::SocketManager>);
}

impl SocketManager {
    pub fn new(protocol: String, hostname: String, port: String, token: String) -> Self {
        glib::Object::builder()
            .property("protocol", protocol)
            .property("hostname", hostname.clone())
            .property("port", port)
            .property("token", token.clone())
            .build()
    }

    fn build_url(&self) -> String {
        let device_id = machine_uid::get().unwrap_or_default();
        format!(
            "{}://{}:{}/socket?api_key={}&device_id={}",
            self.protocol(),
            self.hostname(),
            self.port(),
            self.token(),
            device_id
        )
    }

    pub fn start(&self) {
        let (sender, receiver) = async_channel::bounded::<Message>(1);
        self.imp().message_sender.set(sender).unwrap();

        spawn!(clone!(@weak self as this, @strong receiver => async move {
            let url = this.build_url().clone();
            let (ws_stream, _) = connect_async(url).await.expect("Failed to connect to Jellyfin socket");

            let (mut write, read) = ws_stream.split();

            // Listener subprocess
            spawn!(clone!(@weak this => async move {
                read.for_each(|message| async {
                    match message {
                        Ok(msg) => {
                            if msg.is_text() || msg.is_binary() {
                                match msg.into_text() {
                                    Ok(text) => {
                                        if let Ok(message) = serde_json::from_str::<SocketMessage>(&text) {
                                            if message.message_type == MessageType::ForceKeepAlive {
                                                let interval = message.data.as_i64().unwrap() as u64;
                                                this.send(MessageType::KeepAlive, None);
                                                let previous_keepalive = this.imp().keepalive.replace(Some(KeepAlive::new(Duration::from_secs(interval), this.clone())));
                                                if let Some(previous_keepalive) = previous_keepalive {
                                                    previous_keepalive.stop();
                                                }
                                                this.imp().keepalive.borrow().as_ref().unwrap().start();
                                            } else {
                                                this.emit_by_name::<()>("message", &[&text]);
                                            }
                                        }
                                    },
                                    Err(e) => {
                                        this.emit_by_name::<()>("error", &[&e.to_string()]);
                                    },
                                }
                            } else if msg.is_ping() || msg.is_pong() {
                                match msg.into_text() {
                                    Ok(text) => {
                                        this.emit_by_name::<()>("ping", &[&text]);
                                    },
                                    Err(e) => {
                                        this.emit_by_name::<()>("error", &[&e.to_string()]);
                                    },
                                }
                            } else if msg.is_close() {
                                this.emit_by_name::<()>("stop", &[]);
                            }
                        },
                        Err(e) => {
                            error!("Error: {}", e);
                            this.emit_by_name::<()>("error", &[&e.to_string()]);
                        },
                    }
                }).await;
            }));

            // Sender subprocess
            spawn!(clone!(@strong receiver => async move {
                while let Ok(message) = receiver.recv().await {
                    if let Err(e) = write.send(message).await {
                        error!("Failed sending message to socket: {}", e);
                    }
                }
            }));
        }));
    }

    pub fn send(&self, message_type: MessageType, data: Option<JsonValue>) {
        let data = match data {
            Some(data) => data,
            None => JsonValue::String("".to_string()),
        };
        let message_type = message_type.to_string();
        let message_json = object! {
            MessageType: message_type,
            Data: data,
        };
        let message_string = message_json.dump();
        let message = Message::Text(message_string);

        if let Some(message_sender) = self.imp().message_sender.get() {
            let message_sender = message_sender.clone();
            spawn!(async move {
                message_sender
                    .send(message)
                    .await
                    .expect("The channel needs to be open");
            });
        }
    }

    pub fn connect_message<F: Fn(&Self, String) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("message", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            let response = values[1].get::<String>().unwrap();
            f(&obj, response);
            None
        })
    }

    pub fn connect_ping<F: Fn(&Self, String) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("ping", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            let response = values[1].get::<String>().unwrap();
            f(&obj, response);
            None
        })
    }

    pub fn connect_stop<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("stop", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            f(&obj);
            None
        })
    }

    pub fn connect_error<F: Fn(&Self, String) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_local("error", true, move |values| {
            let obj = values[0].get::<Self>().unwrap();
            let error_str = values[1].get::<String>().unwrap();
            f(&obj, error_str);
            None
        })
    }
}

impl Default for SocketManager {
    fn default() -> Self {
        Self::new(
            "wss".into(),
            "127.0.0.1".into(),
            "8080".into(),
            String::default(),
        )
    }
}

using Gtk 4.0;
using Adw 1;

template $Player: Box {
  orientation: vertical;

  [titlebar]
  Adw.HeaderBar header_bar {}

  Overlay video_overlay {
    child: Box video_frame {
      receives-default: true;
      hexpand: true;
      vexpand: true;
      halign: fill;
      valign: fill;

      Picture video_renderer {
        name: "video";
        hexpand: true;
        vexpand: true;
      }
    };

    [overlay]
    Revealer controls_revealer {
      transition-type: crossfade;
      reveal-child: true;

      GestureClick {
        pressed => $video_clicked() swapped;
      }

      GestureClick {
        touch-only: true;
        pressed => $video_tapped() swapped;
      }

      child: Adw.Clamp controls_clamp {
        maximum-size: 1500;

        child: Box controls_overlay {
          hexpand: true;
          halign: fill;
          valign: end;
          margin-top: 20;
          margin-bottom: 20;
          margin-start: 12;
          margin-end: 12;

          styles [
            "card",
            "view"
          ]

          Box {
            orientation: horizontal;
            hexpand: true;
            halign: fill;
            valign: center;
            spacing: 6;
            margin-top: 12;
            margin-end: 12;
            margin-bottom: 12;
            margin-start: 12;

            Button rewind_button {
              halign: start;
              valign: center;
              icon-name: "skip-backwards-10-symbolic";
              clicked => $seek_backward_clicked() swapped;

              styles [
                "circular"
              ]
            }

            Stack play_pause_stack {
              visible-child: pause_button;
              transition-type: crossfade;
              halign: start;
              valign: center;

              StackPage {
                name: "play";

                child: Button play_button {
                  icon-name: "play-symbolic";
                  clicked => $toggle_playback_button() swapped;

                  styles [
                    "circular"
                  ]
                };
              }

              StackPage {
                name: "pause";

                child: Button pause_button {
                  icon-name: "pause-symbolic";
                  clicked => $toggle_playback_button() swapped;

                  styles [
                    "circular"
                  ]
                };
              }
            }

            Button fast_forward_button {
              halign: start;
              valign: center;
              icon-name: "skip-forward-10-symbolic";
              clicked => $seek_forward_clicked() swapped;

              styles [
                "circular"
              ]
            }

            Separator {
              styles [
                "spacer"
              ]
            }

            Label current_time {
              halign: start;
              valign: center;
              label: "0:00:00";
            }

            Scale seekbar {
              valign: center;
              hexpand: true;
              show-fill-level: true;
              restrict-to-fill-level: false;
              fill-level: 45;
              change-value => $seekbar_value_changed() swapped;
            }

            Label remaining_time {
              halign: start;
              valign: center;
              label: "0:00:00";
            }

            Separator {
              styles [
                "spacer"
              ]
            }

            Button language {
              halign: end;
              valign: center;
              icon-name: "language-symbolic";
              clicked => $language_clicked() swapped;

              styles [
                "flat"
              ]
            }

            VolumeButton volume {
              use-symbolic: true;
              halign: end;
              valign: center;
              focusable: false;
            }

            Button fullscreen {
              halign: end;
              valign: center;
              icon-name: "fullscreen-symbolic";
              clicked => $toggle_fullscreen_button() swapped;

              styles [
                "flat"
              ]
            }
          }
        };
      };
    }
  }
}

Adw.Dialog language_dialog {
  title: _("Audio & Subtitles");

  Adw.ToolbarView {
    [top]
    Adw.HeaderBar {}

    Box {
      orientation: vertical;
      halign: center;
      spacing: 8;
      margin-top: 16;
      margin-bottom: 16;
      margin-start: 16;
      margin-end: 16;

      Box audio_dropdown_container {
        spacing: 4;
        visible: false;
        halign: center;

        Label {
          label: _("Audio:");
        }

        DropDown audio_dropdown {
          model: StringList audio_list {};

          notify::selected => $selected_audio_changed() swapped;

          factory: SignalListItemFactory {
            setup => $setup_dropdown_label() swapped;
            bind => $bind_dropdown_label() swapped;
          };
        }
      }

      Box subtitle_dropdown_container {
        spacing: 4;
        visible: false;
        halign: center;

        Label {
          label: _("Subtitles:");
        }

        DropDown subtitle_dropdown {
          model: StringList subtitle_list {};

          notify::selected => $selected_subtitles_changed() swapped;

          factory: SignalListItemFactory {
            setup => $setup_dropdown_label() swapped;
            bind => $bind_dropdown_label() swapped;
          };
        }
      }
    }
  }
}

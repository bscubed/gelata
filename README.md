# Gelata

*jə-lä′tə* | Etymology: *Latin* | Translates to: *Jelly*

Gelata is a modern and robust Jellyfin client for the Linux desktop.

It aims to provide an elegant and immersive browsing experience for any Jellyfin server with features rivaling that of the default Jellyfin Media Player, all while feeling right at home in your Linux desktop environment.

![screenshot](https://gitlab.com/bscubed/gelata/raw/main/screenshots/gelata-1.png)

## Building

Gelata uses the [meson](http://mesonbuild.com/) build system. Use the following commands to build Gelata from the source directory:

```sh
$ meson build
$ cd build
$ ninja
```

You can also install Gelata system-wide by running:

```sh
$ ninja install
```

To build Gelata locally and run without installing, you can run the following commands:

```sh
$ meson --prefix $PWD/install -Dlocal_install=true build
$ ninja -C build all install
$ XDG_DATA_DIRS=install/share:$XDG_DATA_DIRS LD_LIBRARY_PATH=install/lib64/:$LD_LIBRARY_PATH ./install/bin/gelata
```

## Contributing

Contributions to Gelata are welcome! Refer to the project roadmap below to get an idea of what's currently being worked on before jumping in if you's like, or you can check out the [current issues](https://gitlab.com/bscubed/gelata/issues) for something to tackle.

## Project Roadmap

This project is very much in alpha. Here's a list of what's currently included and some things that are planned for the future.

### Login

| Feature | Completion | Version | Details |
| :--- | :---: | :--- | :--- |
| User Authentication | ✅ | 0.1.0 | User can log into remote Jellyfin server and is assigned a personal authentication token |
| Public User List | ✅ | 0.1.0 | On initial login, a list of configured public users are shown for painless logins |
| HTTPS Server | ✅ | 0.1.0 | Enforces HTTPS connection to server |
| Custom Port | ✅ | 0.1.0 | Can connect to server via a custom port |
| Multiple Servers | ❌ | | Combination of multiple Jellyfin servers/users to switch between |

### Libraries

| Feature | Completion | Version | Details |
| :--- | :---: | :--- | :--- |
| Media List | ✅ | 0.1.0 | Media libraries can be viewed with title, description, and preview icon |
| Movie View | 🚧 | 0.1.0 | Shows most relevant information, but is still missing cast and crew, related movies, and rating information |
| Show List | 🚧 | 0.1.0 | For now, it simply lists all seasons in the show, but will be updated in the future to include more information about the show |
| Season List | 🚧 | 0.1.0 | Currently it simply lists all episodes in the season, but will soon be wrapped into part of the show view in order to simplify the UX |
| Episode List | 🚧 | 0.1.0 | Shows can be viewed with title, release year, and preview icon |
| Artist List | ✅ | 0.1.0 | Artists can be viewed with title, release year, and preview icon |
| Album List | ✅ | 0.1.0 | Albums can be viewed with title, release year, and preview icon |
| Song List | 🚧 | 0.1.0 | Songs can be viewed and selected for playback, but currently in an unintuitive way |
| E-Books | ❌ | | E-Books are unsupported in the current version of Gelata, but are planned for a future release |

### Playback

| Feature | Completion | Version | Details |
| :--- | :---: | :--- | :--- |
| Video Playback | ✅ | 0.1.0 | Movies, TV shows, and videos can be played with pause/play, seek/track, subtitles, fullscreen toggle, and volume control |
| Audio Playback | 🚧 | 0.1.0 | Audio can be played with all functionality listed above, but doesn't show album art or song controls like shuffle/repeat |
| E-Book Reading | ❌ | | E-Books are unsupported in the current version of Gelata, but are planned for a future release |

### Functionality

| Feature | Completion | Version | Details |
| :--- | :---: | :--- | :--- |
| Subtitles | 🚧 | 0.1.0 | Subtitles are loaded by default from OpenSubtitles, but cannot yet be searched for or selected |
| Search | ❌ | | Ability to search all libraries for specific media |
| Filters | ❌ | | Ability to filter lists of media by specific parameters |
| Settings | ❌ | | Settings interface to customize Gelata to your liking |
